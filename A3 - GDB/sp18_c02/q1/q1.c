#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>

uint8_t checkInput(int, char**);
uint16_t getUserInput(char*);
uint16_t takeGuess(uint16_t, uint16_t);
uint8_t handleGuess(uint16_t, uint16_t);
void delay(void);

/**  This program takes an integer between 1 and 10000 inclusive as
 *   command line input, and then attempts to guess the input number
 *   in as few guesses as possible.
 */
int main(int argc, char** argv) {
	
	uint8_t errCode = checkInput(argc, argv);
	
	// Return an error if checkInput() found something wrong
	if (errCode != 0) {
		return errCode;
	} //if
	
	// Get the command line parameter
	uint16_t userInput = getUserInput(argv[1]);
	
	uint16_t highPossible = 10000;
	uint16_t lowPossible = 1;
	uint8_t numGuesses = 0;
	uint8_t guessReturnCode = 0;
	bool success = false;
	
	// Loop until guessing correctly
	while (!success) {
		// Make a guess
		uint16_t g = takeGuess(highPossible, lowPossible);
		
		// Determine whether the guess was correct, too
		// high, or too low
		guessReturnCode = handleGuess(g, userInput);
		numGuesses++;
		
		// Handle the correct, too high, and too low cases
		if (guessReturnCode == 0) {
			success = true;
			printf("I solved your challenge in %d guesses!\n", numGuesses);
		} else if (guessReturnCode == 1) {
			highPossible = g-1;
		} else if (guessReturnCode == -1) {
			lowPossible = g+1;
		} //if-else
		
		//delay();
	} //while
	
	printf("The computer wins again!\n\n");
	
} //main

/**  Check to be sure there are the correct number of args and integer
 *   input in the correct range
 *
 *   Post: returns 0 if input seems good, error code otherwise
 */
uint8_t checkInput(int numargs, char** args) {
	if (numargs != 2) {
		printf("Usage: ./q1 num\n");
		printf("Example: ./q1 51\n");
		return 1;
	} //if
	
    uint16_t intInput = getUserInput(args[1]);
    
    if (intInput < 1 || intInput > 10000) {
		printf("Input argument must be between 1 and 10000, inclusive!\n");
		return 2;
	} //if
		
	return 0;
} //checkInput

/**  Converts char* input into an integer
 *
 *   Post: returns the converted integer
 */
uint16_t getUserInput(char* in) {
	char *p;
    uint16_t intInput = strtol(in, &p, 10);
    return intInput;
} //getUserInput

/**  Compute's the computers next guess at the number
 *
 *   Post: returns the computer's guess to the calling function
 */
uint16_t takeGuess(uint16_t lo, uint16_t hi) {
	uint16_t guess = (hi+lo)/2;
	printf("My guess is %"PRIu16"!\n", &guess);
	return guess;
} //promptGuess

/**  Determines if the computer's guess matches the user's number 
 *
 *   Pre:  receives the computer's guess and the user's number
 *   Post: prints a correct/incorrect message and returns corresponding val
 * 				returns  1 if guess is too high
 * 				returns  0 if guess is correct
 * 				returns -1 if guess is too low
 */
uint8_t handleGuess(uint16_t guess, uint16_t myNum) {
	if (guess == myNum) {
		printf("Hooray!  I guessed correctly!\n\n");
		return 0;
	} else if (guess > myNum) {
		printf("Nope, my guess was too high. I'll try again.\n\n");
		return 1;
	} else {
		printf("Aw, my guess was too low.  I'll try again.\n\n");
		return -1;
	} //if-else
} //handleGuess
