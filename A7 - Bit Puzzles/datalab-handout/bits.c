//    PID          Name
//    -----------------------------------------------------
// 1: paullc      Pau Lleonart Calvo
// 2: foadn       Foad Nachabe

// On my honor:
//
// - I have not discussed the C language code in my program with
// anyone other than my instructor or the teaching assistants
// assigned to this course.
//
// - I have not used C language code obtained from another student,
// the Internet, or any other unauthorized source, either modified
// or unmodified.
//
// - If any C language code or documentation used in my program
// was obtained from an authorized source, such as a text book or
// course notes, that has been clearly noted with a proper citation
// in the comments of my program.
//
// - I have not designed this program in such a way as to defeat or
// interfere with the normal operation of the grading code.
//
// <Pau Lleonart Calvo>
// <paullc>
// <Foad Nachabe>
// <foadn>

/* 
 * CS:APP Data Lab 
 * 
 * <Pau Lleonart Calvo paullc>
 * <Foad Nachabe foadn>
 * 
 * bits.c - Source file with your solutions to the Lab.
 *          This is the file you will hand in to your instructor.
 *
 * WARNING: Do not include the <stdio.h> header; it confuses the dlc
 * compiler. You can still use printf for debugging without including
 * <stdio.h>, although you might get a compiler warning. In general,
 * it's not good practice to ignore compiler warnings, but in this
 * case it's OK.  
 */

#if 0
/*
 * Instructions to Students:
 *
 * STEP 1: Read the following instructions carefully.
 */

You will provide your solution to the Data Lab by
editing the collection of functions in this source file.

INTEGER CODING RULES:
 
  Replace the "return" statement in each function with one
  or more lines of C code that implements the function. Your code 
  must conform to the following style:
 
  int Funct(arg1, arg2, ...) {
      /* brief description of how your implementation works */
      int var1 = Expr1;
      ...
      int varM = ExprM;

      varJ = ExprJ;
      ...
      varN = ExprN;
      return ExprR;
  }

  Each "Expr" is an expression using ONLY the following:
  1. Integer constants 0 through 255 (0xFF), inclusive. You are
      not allowed to use big constants such as 0xffffffff.
  2. Function arguments and local variables (no global variables).
  3. Unary integer operations ! ~
  4. Binary integer operations & ^ | + << >>
    
  Some of the problems restrict the set of allowed operators even further.
  Each "Expr" may consist of multiple operators. You are not restricted to
  one operator per line.

  You are expressly forbidden to:
  1. Use any control constructs such as if, do, while, for, switch, etc.
  2. Define or use any macros.
  3. Define any additional functions in this file.
  4. Call any functions.
  5. Use any other operations, such as &&, ||, -, or ?:
  6. Use any form of casting.
  7. Use any data type other than int.  This implies that you
     cannot use arrays, structs, or unions.

 
  You may assume that your machine:
  1. Uses 2s complement, 32-bit representations of integers.
  2. Performs right shifts arithmetically.
  3. Has unpredictable behavior when shifting an integer by more
     than the word size.

EXAMPLES OF ACCEPTABLE CODING STYLE:
  /*
   * pow2plus1 - returns 2^x + 1, where 0 <= x <= 31
   */
  int pow2plus1(int x) {
     /* exploit ability of shifts to compute powers of 2 */
     return (1 << x) + 1;
  }

  /*
   * pow2plus4 - returns 2^x + 4, where 0 <= x <= 31
   */
  int pow2plus4(int x) {
     /* exploit ability of shifts to compute powers of 2 */
     int result = (1 << x);
     result += 4;
     return result;
  }

FLOATING POINT CODING RULES

For the problems that require you to implent floating-point operations,
the coding rules are less strict.  You are allowed to use looping and
conditional control.  You are allowed to use both ints and unsigneds.
You can use arbitrary integer and unsigned constants.

You are expressly forbidden to:
  1. Define or use any macros.
  2. Define any additional functions in this file.
  3. Call any functions.
  4. Use any form of casting.
  5. Use any data type other than int or unsigned.  This means that you
     cannot use arrays, structs, or unions.
  6. Use any floating point data types, operations, or constants.


NOTES:
  1. Use the dlc (data lab checker) compiler (described in the handout) to 
     check the legality of your solutions.
  2. Each function has a maximum number of operators (! ~ & ^ | + << >>)
     that you are allowed to use for your implementation of the function. 
     The max operator count is checked by dlc. Note that '=' is not 
     counted; you may use as many of these as you want without penalty.
  3. Use the btest test harness to check your functions for correctness.
  4. Use the BDD checker to formally verify your functions
  5. The maximum number of ops for each function is given in the
     header comment for each function. If there are any inconsistencies 
     between the maximum ops in the writeup and in this file, consider
     this file the authoritative source.

/*
 * STEP 2: Modify the following functions according the coding rules.
 * 
 *   IMPORTANT. TO AVOID GRADING SURPRISES:
 *   1. Use the dlc compiler to check that your solutions conform
 *      to the coding rules.
 *   2. Use the BDD checker to formally verify that your solutions produce 
 *      the correct answers.
 */


#endif
    /* Copyright (C) 1991-2012 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */
    /* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */
    /* We do support the IEC 559 math functionality, real and complex.  */
    /* wchar_t uses ISO/IEC 10646 (2nd ed., published 2011-03-15) /
   Unicode 6.0.  */
    /* We do not support C11 <threads.h>.  */
    /* 
 * anyOddBit - return 1 if any odd-numbered bit in word set to 1
 *   Examples anyOddBit(0x5) = 0, anyOddBit(0x7) = 1
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 12
 *   Rating: 2
 */
    int anyOddBit(int x) {

   /**
    * @Author: Pau Lleonart Calvo
    * 
    * We need to see if there are any bits set to 1 in any of the 
    * odd number indeces of an integer. So, we make an 8 bit mask where
    * we set all the odd bits to 1, and all the even bits to 0, like so:
    * 
    * 10101010
    * 
    * This is the largest mask we can set manually because of the restrictions.
    * Then, we left shift this mask by 8 bits and OR it with the 
    * original mask to create a 16 bit mask with 1's in the odd indeces and 0's
    * in the even indeces. We do this again after leftshifting 16 bits to 
    * create a 32 bit alternating mask. 
    * 
    * Now, we can AND the mask and the X 
    * parameter so that if there ARE bits set to 1 in the odd indeces, then 
    * the result of the & operation will be non-zero, or 0 otherwise. We then
    * use the double negation (!!) to create a 0 or 1 output. 
    * 
    */

      int mask = 170;
   mask = (mask << 8) | mask;
   mask = (mask << 16) | mask;

   return !!(mask & x);
}

/* 
 * bitXor - x^y using only ~ and & 
 *   Example: bitXor(4, 5) = 1
 *   Legal ops: ~ &
 *   Max ops: 14
 *   Rating: 1
 */
int bitXor(int x, int y) {

   /*
   @Author: Pau Lleonart Calvo

   An XOR gate can be easily constructed through the logic formula:
   
   A ^ B = (~A & B) | (A & ~B)
   
   In this case, since we are restricted to using ~ and &, we must use NAND gates
   which are universal, meaning we can construct any logical expression with
   them. When we take the expression above and apply DeMorgan's
   Law, we get an expression that uses 5 distinct NAND gates.
   
   We can use the expression above and remove the OR by doing the following:
   
   A | B = ~(~A & ~B)
   
   So the expression becomes:
   
   A ^ B = ~(~(~A & B) & ~(A & ~B))
   
   Thus, we eliminate any illegal operations and are left
   with an XOR gate.
   */

   return ~(~(~x & y) & ~(x & ~y));

}

/* 
 * byteSwap - swaps the nth byte and the mth byte
 *  Examples: byteSwap(0x12345678, 1, 3) = 0x56341278
 *            byteSwap(0xDEADBEEF, 0, 2) = 0xDEEFBEAD
 *  You may assume that 0 <= n <= 3, 0 <= m <= 3
 *  Legal ops: ! ~ & ^ | + << >>
 *  Max ops: 25
 *  Rating: 2
 */
int byteSwap(int x, int n, int m) {

   /*
   @Author: Foad Nachabe

   First we xor the nth byte and then we shift left to nth byte so 
   that we can xor the orginal number. The reason why we do this is b/c
   we want the nth byte to hold the information of the mth byte so in 
   order to do that we xor the nth byte twice which allows it to store
   the information of mth byte. 
   */

   int y = 0;
   n = n << 3;
   m = m << 3;
   y = 0xFF & ((x >> n) ^ (x >> m));
   x = x ^ (y << n);
   x = x ^ (y << m);
   return x;
}

/* 
 * evenBits - return word with all even-numbered bits set to 1
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 8
 *   Rating: 1
 */
int evenBits(void) {

   /**
    * @Author: Pau Lleonart Calvo
    * 
    * Here, we just have to return a 32 bit integer where all the even
    * indeces of bits are set to 1, and all the odd indeces of bits
    * are set to 0.
    * 
    * So, we just create an 8 bit even-alternating mask
    * 01010101
    * 
    * And then left shift it by 8 bits and OR it with the original mask to create
    * a 16 bit even-alternating mask. Then we do the same thing by 16 bits
    * to make a 32 bit even-alternating mask. Then we return it.
    */

   int evenMask = 85;
   evenMask = (evenMask << 8) | evenMask;
   evenMask = (evenMask << 16) | evenMask;
   // printf("EVEN MASK: %d", evenMask);
   return evenMask;
}

/* 
 * fitsShort - return 1 if x can be represented as a 
 *   16-bit, two's complement integer.
 *   Examples: fitsShort(33000) = 0, fitsShort(-32768) = 1
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 8
 *   Rating: 1
 */
int fitsShort(int x) {

   /**
    *  @Author: Pau Lleonart Calvo
    * 
    * We know that the 16 bit two's complement representation of a signed
    * integer has the sign bit (0 or 1) at the 16th bit position. So, if 
    * we take the value x and rightshift it by 31 spots, we get either all
    * 0'ss (positive), or all 1's (negative). If we then take x and rightshift 
    * it by 15 spots, we place the 16th bit (sign bit) and place it at the first
    * position, with everything to the left of it being either 1's or 0's depending
    * on the sign of x.
    * 
    * Then, by XORing x >> 31 and x >> 15, we get whether or not there was a difference
    * between the mask and the 15 righfited value at the very first position, resulting 
    * in a 1 if there is. If a positive value ends up having a difference at that position,
    * then we know that since that is the last spot that would fit in a 16 bit integer,
    * the 0 that makes it positive would not fit in the 16 bits two's complement integer.
    * So, we negate the 1 value (!) to produce the 0 output. Conversely, if the value
    * is negative and there is no difference, we get 0, which we negate to 1, meaning
    * it does fit in a 16 bit two's complement integer.
    * 
    */

   return !((x >> 31) ^ (x >> 15));
}

/* 
 * isAsciiDigit - return 1 if 0x30 <= x <= 0x39 (ASCII codes for characters '0' to '9')
 *   Example: isAsciiDigit(0x35) = 1.
 *            isAsciiDigit(0x3a) = 0.
 *            isAsciiDigit(0x05) = 0.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 15
 *   Rating: 3
 */
int isAsciiDigit(int x) {

   /*
   @Author: Foad Nachabe

   First we create y to be a negative sign. Then if > 0x39 is added the
   sum of the result becomes negative. Then when we add < 0x30 the result 
   is negative. Now add x and use y to check if the upper bound is negative.
   We do the same for the lower bound. If either of the results is negative 
   then it isn't in the range.
   */

   int y = 1 << 31;
   int UB = ~(y | 0x39);
   int LB = ~0x30;
   UB = y & (UB + x) >> 31;
   LB = y & (LB + x + 1) >> 31;
  return !(LB | UB);
}

/* 
 * isNegative - return 1 if x < 0, return 0 otherwise 
 *   Example: isNegative(-1) = 1.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 6
 *   Rating: 2
 */
int isNegative(int x) {

   /**
    * @Author: Pau Lleonart Calvo
    * 
    * Given that the negative of a signed integer is represented using
    * the Two's Complement method, we know that if a signed integer is
    * negative, it must have a 1 in the last bit's position. 
    * 
    * So, we know that because C uses an arithmetic right shift, if 
    * we right shift the value by 31 bits, we are either left with a 0, which 
    * is when the number is positive, or non-zero, which is when the number 
    * is negative.From there we can just use the double negation (!!) to 
    * produce a 0 or 1 return value. 
    */

   return !!(x >> 31);
}

/* 
 * rotateRight - Rotate x to the right by n
 *   Can assume that 0 <= n <= 31
 *   Examples: rotateRight(0x87654321,4) = 0x18765432
 *   Legal ops: ~ & ^ | + << >> !
 *   Max ops: 25
 *   Rating: 3 
 */
int rotateRight(int x, int n) {

   /** 
    * @Author: Pau Lleonart Calvo
    * 
    * Because this function produces a right circular shift, 
    * any bits that come off the right side when shifting must 
    * be placed on the left side instead of being lost. 
    * 
    * So, because we are shifting by n bits, we know that n bits
    * from the right side will come over to the left side, so, we shift
    * the number to the left by 32 (size of the int) - n. Since we can't use
    * the minus (-) operatior, we have to take the two's complement representation
    * (or negative version) of n and add that to 32. Thus, we get the right set
    * of numbers on the left side. 
    * 
    * Then, we know the leftmost bits must be shifted to the right by 
    * n bits. This is more complicated because if x is negative we have 
    * a 1 in the 32th bit spot, which causes an arithmetic right shift, filling
    * the left with 0's. So, when we right shift by n bits, we create a mask
    * with 0's at the n highest bit positions, and then AND that with the 
    * right-shifted number to negate the arithmetic shift. Then we simply OR the
    * left and right sides to combine them into the circular shifted result.
    * 
    */
   
   int result;
   int num = (~n) + 1;
   int rightSide = x << (32 + num); 
   int leftSide = x >> (n);
   int mask = ~((~(1) + 1) << (32 + num));
   leftSide &= mask;
   result = leftSide | rightSide;
   return result;
}