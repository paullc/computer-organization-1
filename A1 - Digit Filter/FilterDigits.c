#include "FilterDigits.h"
#include<stdio.h>
#include<stdlib.h>
/**  Computes a new integer from N by separating all digits of N that
 *   are smaller than a specified value from those that are larger, and 
 *   placing the sets of digits (less, equal, larger) in an order 
 *   specified by one of the FilterAction values. 
 * 
 * For example:
 * 
 *     {954645,  LOFIRST,  6}  -->   544569
 *     {954645,  HIFIRST,  6}  -->   965445
 *     {   333,  LOFIRST,  3}  -->      333
 *     {   333,  HIFIRST,  3}  -->      333
 * 
 * Pre:  N is initialized
 *       action is HIFIRST or LOFIRST
 *       target is between 0 and 9, inclusive
 * Returns:  integer obtained by separating the digits of N as described
 *
 * Restrictions:
 *   - uses only its parameters and local automatic variables
 *     (i.e., no global variables)
 *   - does not make any use of character variables
 *   - does not make any use of arrays
 *   - does not read input or write output
 */

// On my honor:
//
// - I have not discussed the C language code in my program with
// anyone other than my instructor or the teaching assistants
// assigned to this course.
//
// - I have not used C language code obtained from another student,
// the Internet, or any other unauthorized source, either modified
// or unmodified.
//
// - If any C language code or documentation used in my program
// was obtained from an authorized source, such as a text book or
// course notes, that has been clearly noted with a proper citation
// in the comments of my program.
//
// - I have not designed this program in such a way as to defeat or
// interfere with the normal operation of the grading code.
//
// <Pau Lleonart Calvo>
// <paullc>

uint32_t FilterDigits(uint32_t N, enum FilterAction action, uint8_t pivot) {

   if (N == 0) {
      return N;
   }

   uint32_t newNum = 0;

   // Slot represents the position in the number
   // where the current digit will be added. The loops
   // increment this by 10* each loop in order to shift
   // the current position leftwards by 1. 

   int slot = 1;
   
   switch (action)
   {
      // Case where must sort with smaller numbers to left and 
      // larger numbers to right.
      case LOFIRST:
         
         // First loop checks for numbers larger than pivot
         // and adds them to the rightmost spots
         for (int numCopy = N; numCopy > 0; numCopy /= 10) {
            if (numCopy % 10 > pivot) {
               // Add current number to running newNum sum at
               // the correct position by multing by sum
               newNum += (numCopy % 10) * slot;
               slot *= 10;
            }
         }

         // Second loop checks for the numbers equal to the pivot
         // and adds them to the center spots
         for (int numCopy = N; numCopy > 0; numCopy /= 10) {
            if (numCopy % 10 == pivot) {
               newNum += (numCopy % 10) * slot;
               slot *= 10;
            }
         }

         // Last loop checks for numbers smallert than the pivot
         //  and adds them to the leftmost spots
         for (int numCopy = N; numCopy > 0; numCopy /= 10) {
            if (numCopy % 10 < pivot) {
               newNum += (numCopy % 10) * slot;
               slot *= 10;
            }
         }
         break;
      // Case where must sort with larger numbers to left and 
      // smaller numbers to right.
      case HIFIRST:

         // First loop checks for numbers larger than pivot
         // and adds them to the leftmost spots
         for (int numCopy = N; numCopy > 0; numCopy /= 10) {
            if (numCopy % 10 < pivot) {
               newNum += (numCopy % 10) * slot;
               slot *= 10;
            }
         }

         // Second loop checks for the numbers equal to the pivot
         // and adds them to the center spots
         for (int numCopy = N; numCopy > 0; numCopy /= 10) {
            if (numCopy % 10 == pivot) {
               newNum += (numCopy % 10) * slot;
               slot *= 10;
            }
         }

         // Last loop checks for numbers smallert than the pivot
         //  and adds them to the rightmost spots
         for (int numCopy = N; numCopy > 0; numCopy /= 10) {
            if (numCopy % 10 > pivot) {
               newNum += (numCopy % 10) * slot;
               slot *= 10;
            }
         }
         break;
      default:
         return 0;
   }
   
   return newNum;
}

