#include<stdio.h>
#include<stdlib.h>

// On my honor:
//
// - I have not discussed the C language code in my program with
// anyone other than my instructor or the teaching assistants
// assigned to this course.
//
// - I have not used C language code obtained from another student,
// the Internet, or any other unauthorized source, either modified
// or unmodified.
//
// - If any C language code or documentation used in my program
// was obtained from an authorized source, such as a text book or
// course notes, that has been clearly noted with a proper citation
// in the comments of my program.
//
// - I have not designed this program in such a way as to defeat or
// interfere with the normal operation of the grading code.
//
// <Pau Lleonart Calvo>
// <paullc>

int main(int argc, char **argv) {
    // Open files to be read from and written to
    FILE *input = fopen(argv[1], "r");
    FILE *output = fopen(argv[2], "w");

    // Exit if number of args is too few to function
    if (argc < 3) {
        exit(1);
    }

    // If either of the files is null, exit
    if (input == NULL || output == NULL)
    {
        printf("Error opening file!\n");
        fclose(input);
        fclose(output);
        exit(1);
    }

    // Set up the integer arrays to store parsed data
    int crd1a[3], crd1b[3], crd2a[3], crd2b[3];
    char comp1a, comp1b, comp2a, comp2b;

    // Prints the two first ungraded standard lines
    fprintf(output, "First coordinate                  Second coordinate                 seconds     DMS\n");
    fprintf(output, "------------------------------------------------------------------------------------------\n");
    // While the scanner returns the proper number of scanned elements, print each new line
    // of calculated data.
    while (fscanf(input,"(%3d%2d%2d%c, %2d%2d%2d%c)\t(%3d%2d%2d%c, %2d%2d%2d%c)\n",
        &crd1a[0], &crd1a[1], &crd1a[2], &comp1a,
        &crd1b[0], &crd1b[1], &crd1b[2], &comp1b,
        &crd2a[0], &crd2a[1], &crd2a[2], &comp2a,
        &crd2b[0], &crd2b[1], &crd2b[2], &comp2b) == 16) {
        
        // Calculate distance x
        int distanceA = ((crd1a[0] * 3600) + (crd1a[1] * 60) + crd1a[2]) - 
            ((crd2a[0] * 3600) + (crd2a[1] * 60) + crd2a[2]);
        
        // Calculate distance y
        int distanceB = ((crd1b[0] * 3600) + (crd1b[1] * 60) + crd1b[2]) - 
            ((crd2b[0] * 3600) + (crd2b[1] * 60) + crd2b[2]);
        
        // Take absolute value of the operations
        if (distanceA < 0) {
            distanceA *= -1;
        }
        if (distanceB < 0) {
            distanceB *= -1;
        }

        // Total taxi distance is the sum of the x and y distances
        // Then convert total seconds to d m s format. 
        int taxiSeconds = distanceA + distanceB;
        int days = (taxiSeconds / 3600);
        int minutes = (taxiSeconds % 3600) / 60;
        int seconds = (taxiSeconds % 3600) % 60;

        // Print the calculated output to a file. 
        fprintf(output, "(%dd %02dm %02ds %c, %dd %02dm %02ds %c)   (%dd %02dm %02ds %c, %dd %02dm %02ds %c)    %d  %dd %02dm %02ds\n", 
        crd1a[0], crd1a[1], crd1a[2], comp1a,
        crd1b[0], crd1b[1], crd1b[2], comp1b,
        crd2a[0], crd2a[1], crd2a[2], comp2a,
        crd2b[0], crd2b[1], crd2b[2], comp2b,
        taxiSeconds, days, minutes, seconds);
    }

    // Close opened files. 
    fclose(input);
    fclose(output);
}
