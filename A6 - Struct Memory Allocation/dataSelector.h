#ifndef DATASELECTOR_H
#define DATASELECTOR_H
#include <stdio.h>
#include <inttypes.h>

/***  DO NOT MODIFY THIS FILE IN ANY WAY!! ***/

uint32_t loadRecords(FILE* fp);
char* selectTestCase();
void clearRecords();

#endif
