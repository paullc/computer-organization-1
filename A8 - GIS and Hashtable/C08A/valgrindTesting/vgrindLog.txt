==16921== Memcheck, a memory error detector
==16921== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==16921== Using Valgrind-3.15.0-608cb11914-20190413 and LibVEX; rerun with -h for copyright info
==16921== Command: ./c08a_StringHashTable NM_1000.txt -repeat
==16921== Parent PID: 16920
==16921== 
--16921-- 
--16921-- Valgrind options:
--16921--    --leak-check=full
--16921--    --show-leak-kinds=all
--16921--    --log-file=vgrindLog.txt
--16921--    --track-origins=yes
--16921--    -v
--16921-- Contents of /proc/version:
--16921--   Linux version 3.10.0-1062.9.1.el7.x86_64 (mockbuild@kbuilder.bsys.centos.org) (gcc version 4.8.5 20150623 (Red Hat 4.8.5-39) (GCC) ) #1 SMP Fri Dec 6 15:49:49 UTC 2019
--16921-- 
--16921-- Arch and hwcaps: AMD64, LittleEndian, amd64-cx16-rdtscp-sse3-ssse3-avx-f16c-rdrand
--16921-- Page sizes: currently 4096, max supported 4096
--16921-- Valgrind library directory: /usr/libexec/valgrind
--16921-- Reading syms from /home/ugrads/nonmajors/paullc/2505/A8 - GIS and Hashtable/C08A/valgrindTesting/c08a_StringHashTable
--16921-- Reading syms from /usr/lib64/ld-2.17.so
--16921-- Reading syms from /usr/libexec/valgrind/memcheck-amd64-linux
--16921--    object doesn't have a symbol table
--16921--    object doesn't have a dynamic symbol table
--16921-- Scheduler: using generic scheduler lock implementation.
--16921-- Reading suppressions file: /usr/libexec/valgrind/default.supp
==16921== embedded gdbserver: reading from /tmp/vgdb-pipe-from-vgdb-to-16921-by-paullc-on-holly.rlogin
==16921== embedded gdbserver: writing to   /tmp/vgdb-pipe-to-vgdb-from-16921-by-paullc-on-holly.rlogin
==16921== embedded gdbserver: shared mem   /tmp/vgdb-pipe-shared-mem-vgdb-16921-by-paullc-on-holly.rlogin
==16921== 
==16921== TO CONTROL THIS PROCESS USING vgdb (which you probably
==16921== don't want to do, unless you know exactly what you're doing,
==16921== or are doing some strange experiment):
==16921==   /usr/libexec/valgrind/../../bin/vgdb --pid=16921 ...command...
==16921== 
==16921== TO DEBUG THIS PROCESS USING GDB: start GDB like this
==16921==   /path/to/gdb ./c08a_StringHashTable
==16921== and then give GDB the following command
==16921==   target remote | /usr/libexec/valgrind/../../bin/vgdb --pid=16921
==16921== --pid is optional if only one valgrind process is running
==16921== 
--16921-- REDIR: 0x4019e30 (ld-linux-x86-64.so.2:strlen) redirected to 0x580c7ed5 (???)
--16921-- REDIR: 0x4019c00 (ld-linux-x86-64.so.2:index) redirected to 0x580c7eef (???)
--16921-- Reading syms from /usr/libexec/valgrind/vgpreload_core-amd64-linux.so
--16921-- Reading syms from /usr/libexec/valgrind/vgpreload_memcheck-amd64-linux.so
==16921== WARNING: new redirection conflicts with existing -- ignoring it
--16921--     old: 0x04019e30 (strlen              ) R-> (0000.0) 0x580c7ed5 ???
--16921--     new: 0x04019e30 (strlen              ) R-> (2007.0) 0x04c2d1b0 strlen
--16921-- REDIR: 0x4019db0 (ld-linux-x86-64.so.2:strcmp) redirected to 0x4c2e300 (strcmp)
--16921-- REDIR: 0x401aa70 (ld-linux-x86-64.so.2:mempcpy) redirected to 0x4c31f90 (mempcpy)
--16921-- Reading syms from /usr/lib64/libc-2.17.so
--16921-- REDIR: 0x4ec7130 (libc.so.6:strcasecmp) redirected to 0x4a247a0 (_vgnU_ifunc_wrapper)
--16921-- REDIR: 0x4ec3eb0 (libc.so.6:strnlen) redirected to 0x4a247a0 (_vgnU_ifunc_wrapper)
--16921-- REDIR: 0x4ec9400 (libc.so.6:strncasecmp) redirected to 0x4a247a0 (_vgnU_ifunc_wrapper)
--16921-- REDIR: 0x4ec6910 (libc.so.6:memset) redirected to 0x4a247a0 (_vgnU_ifunc_wrapper)
--16921-- REDIR: 0x4ec68c0 (libc.so.6:memcpy@GLIBC_2.2.5) redirected to 0x4a247a0 (_vgnU_ifunc_wrapper)
--16921-- REDIR: 0x4ec58a0 (libc.so.6:__GI_strrchr) redirected to 0x4c2cb70 (__GI_strrchr)
--16921-- REDIR: 0x4ec2330 (libc.so.6:strcmp) redirected to 0x4a247a0 (_vgnU_ifunc_wrapper)
--16921-- REDIR: 0x4f75d90 (libc.so.6:__strcmp_sse42) redirected to 0x4c2e2b0 (__strcmp_sse42)
--16921-- REDIR: 0x4ebc6b0 (libc.so.6:malloc) redirected to 0x4c29eec (malloc)
--16921-- REDIR: 0x4edc920 (libc.so.6:__GI_strstr) redirected to 0x4c32220 (__strstr_sse2)
--16921-- REDIR: 0x4ebcad0 (libc.so.6:free) redirected to 0x4c2afe6 (free)
--16921-- REDIR: 0x4ec6ae0 (libc.so.6:__GI_mempcpy) redirected to 0x4c31cc0 (__GI_mempcpy)
--16921-- REDIR: 0x4ecd3c0 (libc.so.6:strchrnul) redirected to 0x4c31ab0 (strchrnul)
--16921-- REDIR: 0x4ebd0d0 (libc.so.6:calloc) redirected to 0x4c2bff3 (calloc)
--16921-- REDIR: 0x4ec5fa0 (libc.so.6:memchr) redirected to 0x4c2e3a0 (memchr)
--16921-- REDIR: 0x4ecbb40 (libc.so.6:__GI_memcpy) redirected to 0x4c2efb0 (__GI_memcpy)
--16921-- REDIR: 0x4ec3d80 (libc.so.6:strlen) redirected to 0x4a247a0 (_vgnU_ifunc_wrapper)
--16921-- REDIR: 0x4fa6680 (libc.so.6:__strlen_sse2_pminub) redirected to 0x4c2d0f0 (strlen)
--16921-- REDIR: 0x4ebcbb0 (libc.so.6:realloc) redirected to 0x4c2c1c5 (realloc)
--16921-- REDIR: 0x4ecd1b0 (libc.so.6:__GI___rawmemchr) redirected to 0x4c31b10 (__GI___rawmemchr)
--16921-- REDIR: 0x4ec37c0 (libc.so.6:strcpy) redirected to 0x4a247a0 (_vgnU_ifunc_wrapper)
--16921-- REDIR: 0x4ed16e0 (libc.so.6:__strcpy_sse2_unaligned) redirected to 0x4c2d1d0 (strcpy)
--16921-- REDIR: 0x4ecbad0 (libc.so.6:memcpy@@GLIBC_2.14) redirected to 0x4a247a0 (_vgnU_ifunc_wrapper)
--16921-- REDIR: 0x4f8ba40 (libc.so.6:__memcpy_ssse3_back) redirected to 0x4c2e7b0 (memcpy@@GLIBC_2.14)
--16921-- REDIR: 0x4ec3dd0 (libc.so.6:__GI_strlen) redirected to 0x4c2d110 (__GI_strlen)
==16921== 
==16921== HEAP SUMMARY:
==16921==     in use at exit: 0 bytes in 0 blocks
==16921==   total heap usage: 10,047 allocs, 10,047 frees, 992,554 bytes allocated
==16921== 
==16921== All heap blocks were freed -- no leaks are possible
==16921== 
==16921== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
