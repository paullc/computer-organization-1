Hash table contains the following 942 entries:
    0:      Datil Well Campground:    [50499]

    1:      Silver Snowplay Area:    [42290]
            Cole Spring Picnic Area:    [8032]

    2:      Cooks Cabin:    [90855]
            Tierra Monte Subdivision:    [8608]

    3:      Sage Trick Tank:    [119988]
            Olguin Pit Tanks:    [94331]
            Warm Spring:    [93887]
            Hidden Tank:    [92987]
            Coyote Tank:    [65423, 86661]
            South East Tank:    [35537]
            Stone Tank:    [19419]

    4:      Tully Well:    [37300]
            Alamo Midway Airport:    [25379]
            West Side Tank North:    [14950]
            Bosque Trail:    [5491]

    7:      Smoothing Iron Tank:    [135848]
            Upper Tank:    [64619, 134525]
            Section 24 Tank:    [40165]
            Rock Water Holes:    [33779]
            West Silver Tank:    [11030]

    8:      Stewart Corral:    [113261]

    9:      Pit Tank Number Three:    [74652]

   11:      Upper Barbero Tank:    [93763]
            Drift Fence Tank:    [90577]
            Taoses Tank:    [60126]
            Rawhide Water Storage Tank:    [23569]

   12:      Bancos Loop:    [76018]

   15:      Center Tank:    [135177]
            Wet Weather Spring:    [116482]
            Four Tank:    [109982]
            Campground Spring:    [102331]
            Rincon Spring:    [96999]
            Rock Header Tank:    [82435]
            Ryan Spring:    [69977]
            West Tank:    [37929, 38187]
            Hawkins Spring:    [6851]

   16:      Quail Pot Well:    [84147]

   17:      Iron Gate Recreation Site:    [85735]
            Flechado Recreation Site:    [68682]

   18:      Aspen Forrest Service Station:    [54995]

   19:      Upper Abrevadero Tank:    [121401]
            Rowden Tank:    [110259]
            Martinez Tanks:    [101951]
            Fowler Spring:    [82162]
            New Tank:    [40850, 125662]
            Kline Tank:    [26834]
            Lost Spring:    [25779]

   20:      Thomas Martinez O'Reilly Cow Camp:    [46127]

   21:      Bean Patch Tank Number One:    [113832]
            Pine Flat Picnic Area:    [7513]

   23:      Eleven Tank:    [117700]
            Navajo Tank:    [103448]
            Mountain Lion Peak:    [43484]
            Funderburg Tank Number Two:    [26031]

   24:      Petrero Waterfall:    [122914]
            Barbero Well:    [93645]
            Big Tesuque Campground:    [50364]

   25:      USC and GS Laboratory Seismological Laboratory:    [49078]

   26:      Jicarilla Ranger Station:    [53020]
            Muleshoe Canyon:    [24928]
            Left Hand Sapello River:    [2981]

   27:      Gap Pough Spring:    [133447]
            Burnt Spring:    [85606]
            Lee Roy Tank:    [61975]

   31:      Trap Tank:    [134649]
            Booth Hollow Tank:    [133185]
            Garcia Tank Number Two:    [96029]
            Rojo Tank:    [67785]
            Shack Tank:    [27079]

   33:      Trick Tank Number One:    [121110]
            Carson Supervisors Office:    [71326]

   34:      Smokey Bear Ranger Station:    [32217]

   35:      Brannon Spring:    [92035]
            Little Rosa Spring:    [78357]
            Mesa Tank:    [35678, 133875]

   36:      Gut Ache Corral:    [15791]

   37:      Lopez Lake:    [52476]

   39:      Whitecap Tank:    [80115]
            Petaca Trick Tank:    [67231]
            La Cueva Number Two Spring:    [59159]
            Stanley Tank:    [47849, 114154]
            Buzzard Tank:    [29758]
            Lawyer Dirt Tank:    [23309]
            Wright Spring:    [21693]
            Line Gap Tank:    [16183]

   40:      Ojo Corral:    [104024]

   41:      Hopewell Ridge:    [131580]

   42:      Magdalena Canyon:    [19263]

   43:      Arroyo Seco Spring:    [123051]
            Llamo Tank:    [111865]
            Flat Tank:    [90720]
            Pigeon Tank:    [83206]
            Lawson Spring:    [31626]
            Old Highway Tank:    [16044]

   45:      Trick Tank Number Three:    [120412]
            Tierra Azul Picnic Area:    [71966]

   47:      Jay Tank:    [135442]
            North Tank:    [116617]
            Cuchilla Tank:    [96317]
            Elk Tank:    [90442]
            North Star Tank:    [89321]
            Sarca Spring:    [54196, 102886]
            Railsplitter Tank Number Two:    [43902]
            Rock Draw Tank:    [39479]
            West Mountain Trick Tank:    [38577]

   48:      Amador Windmill:    [123350]
            Mora Campground:    [85876]
            Schell:    [49361]
            Hatt Ranch:    [39232]

   51:      Flanigan Spring:    [133314]
            Sage Tank:    [119293]
            Easy Tank:    [114899]
            Cochina Spring:    [100372, 100505]
            Twin Ponds Tank:    [99971]
            Turpin Tank:    [29510]
            Hackberry Spring:    [24379]
            Acrey Water Storage Tank:    [22766]

   54:      Hearn:    [22641]
            North Fork Negro Ed Canyon:    [20673]

   55:      Lower San Antone Tank:    [115467]
            Bell Spring:    [81355]

   56:      Brand Ranch:    [118204]
            Schmidt Ranch:    [106874]
            Upper Corral:    [91256]

   58:      South Fork Negro Ed Canyon:    [20853]

   59:      Venado Spring:    [71840]
            Wildlife Trick Tank:    [58644]

   60:      Hopewell Lake Campground:    [131852]

   61:      The Rinconada:    [4963]

   63:      Sanchez Trough Spring:    [79709]
            Pine Canyon Tank:    [50989]
            Griffen Spring:    [20111]
            West Spur Tank:    [11712]
            Dry Tank:    [10780]

   64:      San Jose Church:    [82814]

   67:      Buzzard Branch Tank:    [134139]
            Fickle Tank:    [105942]
            JR Tank:    [105270]

   68:      Berryman Ranch:    [117182]

   69:      Monastery Lake:    [59447]

   71:      Henry Spring:    [124600]
            South Salitral Trick Tank:    [103167]
            Cisneros Park Spring:    [97399]
            Road Mesa Trick Tank:    [75744]
            Prairie Tank:    [65685]
            Ambrose Tank:    [62675]
            Lower Snow Tank:    [40583]
            Graveyard Tank:    [40305]

   72:      Woodbury Ranch:    [116756]

   73:      Santa Fe Trail Rest Area:    [53163]

   75:      Quail Tank:    [132423]
            Frenchy Tank:    [127845]
            Canada Spring:    [99304]
            Vigas Spring:    [52083]
            Perry Canyon Tank:    [17510]
            Glover Spring:    [7119]
            Romero Tank:    [417]

   78:      Ramerez:    [9569]

   79:      Stateline Water Storage Tank:    [134905]
            Copper Trick Tank:    [120703]
            Dead End Tank:    [120563]
            ET Tank Number Two:    [92163]
            Aquate Tank:    [60414]
            Aqropyron Tank:    [59983]
            Bell Tank:    [9826]

   80:      Shartzer Windmill:    [46407]

   83:      Espinosa Tank:    [131027]
            One Tank:    [127002]
            North Trick Tank:    [95888, 109848]
            Rim Rock Tank:    [94482]
            Boulder Tank:    [83066]
            Taylor Tanks:    [24249]

   84:      Red John Well:    [87189]
            T Bone Ranch:    [74936]

   85:      Loco Ridge:    [65562]

   87:      Redondo Peak:    [83617]
            Miranda Trick Tank:    [72102]
            Water Hole Tank:    [34181]

   88:      Hines Ranch:    [46794]
            Lamay Ranch:    [33533]
            Devils Park Corral:    [14533]

   91:      Section Fifteen Tank:    [71453]
            Maestas Spring:    [67913]
            Antelope Tank:    [41210]
            Black Water Storage Tank:    [23840]

   92:      Pole Corral:    [113399]
            Erramouspe Ranch:    [49977]

   93:      Arroyo Noria:    [76593]
            Aspen Basin Recreation Area:    [55280]

   94:      Walker:    [50624]

   95:      Ancones Trick Tank:    [121830]
            Rogers Tank:    [30173]
            SA Tank Number Two:    [26184]
            Indian Tank:    [13828]
            Stanton Tank:    [11852]

   97:      Winsor Creek Recreation Site:    [85199]

   99:      Krause Tank:    [126591]
            Ancones Tank:    [121974]
            Deep Canyon Tank:    [83875]
            Viego Tank:    [73711]
            Hermit Spring:    [57423]
            Negro Tank:    [56235, 62254, 98278, 111034]
            Las Uvas Spring:    [47184]
            Montgomery Water Storage Tank:    [23974]
            Little Joe Tank:    [16976]

  100:      Buck Windmill:    [64492]
            Clayton Junior High School:    [53416]

  101:      Bert Clancy Recreation Site:    [58226]
            Albuquerque Tree Nursery:    [8171]

  103:      Burned Mountain Tank:    [132138]
            Number Two Tank:    [115911]
            Seven Spring:    [79854]
            Gonzales Tank:    [39098]
            Pines Tank:    [34464]

  107:      Cunningham Tank:    [130737]
            Vibora Tank:    [122238]
            Lucky Tank:    [112557]
            Pino Tank:    [7383]

  108:      KOA Campground:    [52746]

  109:      Mule Ridge:    [29249]

  111:      South West Tank:    [35126]

  112:      Rocky Canyon Campground:    [90036]
            Red Sap Well:    [87696]

  113:      La Junta Summerhome Area:    [69844]

  115:      Mine Tank:    [107905]
            Pit Tank:    [75061, 97838]
            Creek Tank:    [40974]

  119:      Vigil Tank:    [134007]
            Guardia Spring:    [101029]
            Bonita Tank:    [100761]
            Water Spring:    [81628]
            Section Thirty-five Tank:    [76453]
            Long Canyon Tank:    [75195]
            Orono Spring:    [26467]
            Jake Tank:    [20545]
            Cowboy Tank:    [13686]
            Elumpe Tank:    [11307]

  121:      Timberon Lodge:    [30450]
            Red Mesa:    [3862]

  123:      Trick Tank Number Two:    [94630]
            Deer Tank:    [63796, 106341, 129935]
            Boles Acres:    [25234]
            Turkey Tank:    [10233, 13404, 106997]

  124:      Bear Trap Corral:    [87566]
            Fourth of July Campground:    [6299]
            Romero Tract:    [3573]

  125:      Taos Administration Site:    [70784]

  127:      Angel Tank:    [136122]
            West Trick Tank:    [103877]
            Valle Tank:    [97981]
            Rock Slide Spring:    [90313]
            Morman Tank:    [63932]
            Foster Water Storage Tank:    [23705]
            Lewis Tank:    [14259, 17104]

  130:      Gobbler Knob:    [22115]

  131:      Amos Tank:    [130076]
            Jacal De Palo Spring:    [123200]
            Alamosa Number Two Tank:    [118864]
            Jaspe Trick Tank:    [103729]
            Kiowa Tank:    [66244]
            No Agua Tank:    [60693]
            Indian Peak Tank:    [15652]

  132:      Rendija Trail:    [48401]

  133:      Zamora:    [110393]

  134:      Rio Grande Wild River:    [3442]

  135:      Luterio Spring:    [100895]
            Skates Tank Number Two:    [88621]
            Red Rock Tank:    [86524]
            Hay Tank:    [27627]
            Duran Tank:    [2229, 131312]

  136:      Steel Windmill:    [91508]
            Natural Arch Trail:    [48531]

  137:      El Rito District Ranger Office:    [119152]

  138:      Red Lick Canyon:    [45405]

  139:      ET Tank:    [107513]
            Martin Spring:    [77936]
            Reynolds Tank:    [61698]
            Well Number Two:    [42666]
            West Sign Camp Tank:    [13254]

  140:      Driveway Windmill:    [54609]

  143:      Pedernal Trick Tank:    [125134]
            Dry Spring:    [82027]
            Piedra Lisa Spring:    [48800]
            Tully Tank:    [37546]
            Tony Tank:    [31110]
            Frisco Spring:    [18350]
            Sheep Seep Tank:    [17237]

  144:      Terry Well:    [88370]
            Panchuela Campground:    [85069]

  146:      Dark Canyon Lookout Tower:    [29373]

  147:      Pipeline Spring:    [77532]
            La Cueve Number One Spring:    [59303]
            End of Road Tank:    [17937]

  151:      Puertecito Rim Tank:    [77669]
            Spring Canyon Spring:    [36080]

  152:      Vallecitos Trail:    [4661]

  155:      Hinds Tank:    [117833]
            Ojitos Spring:    [102602]
            Switch Spring:    [78880]
            Fox Tank:    [32754]
            Negrito Tank:    [17375, 25648]
            Roadside Number One Tank:    [855]

  156:      Flowing Well:    [57173]
            Lemitas Trail:    [4277]

  157:      Las Tablas Site 1:    [38840]
            Wofford Electronic Site:    [22244]

  159:      Palome Tank:    [116056]

  162:      Piedra Lumbre Canyon:    [4794]

  163:      Comanche Number Two Tank:    [67507]

  166:      NASA Wind Charger:    [53293]

  167:      Martinez Tank:    [66797]
            Cher Tank:    [45715]

  169:      Huero Lake:    [69200]

  171:      Jarosito Spring:    [101295]
            Upper Trick Tank:    [96176]
            Rest Area Trick Tank:    [68420]
            Cerro del Oso:    [57041]
            Guaye Trick Tank:    [54740]
            East Tank:    [10646, 38448, 109587]

  175:      Liza Tank:    [133743]
            Daggett Tank:    [110119]
            Mesa Alta Wildlife Trick Tank:    [104716]
            Game Department Tank:    [98855, 115320]
            Slow Tank:    [91900]
            Orejas Tank:    [72916]
            Corrales Spring:    [57958]
            Graveyard Spring:    [43610]

  176:      Walking Beam Windmill:    [79017]
            Bagley Ranch:    [46538]

  177:      Big Pine Picnic Area:    [74113]
            Capulin Snow Play Area:    [9180]

  179:      Nelson Wells:    [124341]
            Owl Trick Tank:    [67659]
            Horse Tank:    [58898]
            Drake Tank:    [56372]
            Morphy Lake State Park:    [55421]
            Gum Spring:    [38712]
            Perry Tank:    [34593]
            Stewart Meadows:    [5091]
            Alive Number Two Tank:    [705]

  180:      Pinyon Corral:    [86796]
            Wasp Windmill:    [82940]
            West Side Tank South:    [15097]

  181:      Winsor Creek Summer Home:    [85336]
            Tule Lake:    [42419]

  182:      Chalon:    [105154]

  183:      Number Five Tank:    [115038]
            Maverick Tank:    [89181]
            Sunflower Tank:    [41347]
            Cougar Tank:    [12821]

  184:      Mouth of Bear Ranch:    [83345]
            Pumice Mine Pump:    [54074]
            Cibola Scout Camp:    [19856]

  185:      Little Korea:    [69328]
            KRRR Radio Tower (Conway):    [31762]

  187:      Claverine Tank:    [127697]
            Chupadero Spring:    [74248]

  188:      Coe Well:    [34867]
            Summit House Restaurant:    [8905]
            Presbyterian Church Camp:    [7775]

  189:      Petaca Tank A:    [67366]

  190:      Petaca Tank B:    [67090]
            Maxon:    [30710]

  191:      Rattle Tank:    [102468]
            Petaca Tank C:    [72517]
            Monticello Number One Tank:    [48133]
            Potato Tank:    [22376]

  193:      Monastery Lake Recreation Site:    [59705]

  194:      North Fork Canyon:    [32055]

  195:      Navajo Trick Tank:    [103591]
            Railroad Spring:    [88492]
            Upper Pigeon Tank:    [86920]
            Road Canyon Tank:    [63232]
            Escondida Canyon Tank:    [51795]
            Madera Tank:    [51133]
            Ivans Tank:    [40724]
            Yates Tank:    [35401]
            North Bosque Peak:    [5228]

  196:      Hart Trail:    [109724]
            Hidden Valley Ranch:    [57552]
            Aqua Dulce Seep:    [29111]

  199:      Eagle Spring:    [77406]
            Crooked Water Storage Tank:    [41486]
            Rock Tank:    [11578]
            Albuquerque Trail Spring:    [6587]

  200:      Drift Fence Well:    [87061]
            Cowles Campground:    [85479]

  203:      Dead Horse Spring:    [104152]
            Cruz Number One Tank:    [95480]
            Boundary Tank:    [78071, 89463]
            Alive Number One Tank:    [1291]
            Casas Tank:    [1152]

  204:      Old Tank Corral:    [84018]

  207:      McKenna Mesa Tank:    [92304]
            Lower Tank:    [64756, 106741]
            Forks Tank:    [22509]
            Little Tank:    [9961, 38966, 39886, 44472, 110772, 117568]

  208:      Trick Tank Number Six:    [95047]
            Chimayo Boy Scout Camp:    [53816]

  209:      Dry Lake:    [56790]
            Apache Forest Service Facility:    [52866]

  210:      High Clark Canyon:    [133580]

  211:      Sheep Tank:    [112419]
            Pit Tank Number Two:    [105526]
            Schofield Tank:    [64070]
            Puye Cliff Dwellings:    [54867]
            Ojito Los Burros Spring:    [5881]

  212:      Caja Well:    [48282]

  213:      Christ of the Desert Monastary:    [129108]

  215:      Pinon Number Two Tank:    [119426]
            Number One Tank:    [116348]
            Amarilla Tank:    [111312]
            Trail Spring:    [81890]

  217:      BLM Recreation Site:    [53946]

  218:      Carson Forest Service Station:    [70909]
            Santa Fe Ski Basin:    [53684]

  219:      Middle Canyon Tank:    [131172]
            Slater Tank:    [122643]
            Sandlin Tank:    [122112]
            Casey Spring:    [104430]
            Trough Spring:    [83479]
            Norski Cross Country Ski Track:    [55137]
            Cripple Saddle Tank:    [14114]

  220:      Brooks Well:    [41100]

  223:      Nick Spring:    [102088]
            Clear Tank:    [92586]
            Hub Tank:    [87969]
            Cruces Basin Wilderness:    [56651]
            Big Chief Water Storage Tank:    [41889]
            S U Spring:    [18870]

  224:      Brush Ranch:    [57831]

  227:      Tank Twenty-two:    [118323]
            East Sisneras Spring:    [112969]
            Miller Scott Spring:    [108172]
            Red Cloud Trick Tank:    [49839]
            Roadside Number Two Tank:    [2368]

  228:      Eds Windmill:    [91382]
            Tule Well:    [42548]

  231:      Railsplitter Number One Tank:    [44212]

  232:      Pinon Well:    [117965]
            Smith Well:    [43237]

  233:      Oak Flats Picnic Area:    [93256]
            Camp Summer Life:    [68950]
            Corner Tank Number One:    [28181]
            Ninemile Picnic Area:    [9045]

  235:      Booth Hollow Spring:    [132802]
            Powerline Tank:    [128409]
            Snag Tank:    [117437]
            Bryd Tank:    [77152]
            Road Tank:    [11172, 79570]

  236:      South Well:    [42030]

  237:      Reilsplitter Tank Number Three:    [44056]

  239:      Rincon Tank:    [118595]
            Casa Tank:    [107263]
            Mesa Ojitos Trick Tank:    [97266]
            Cooney Spring:    [79289]
            Royals Tank:    [66382]
            Palmer Tank:    [59844]
            Pauls Tank:    [33914]
            Willow Spring Tank:    [18722]

  240:      Encino Corral:    [125270]
            Sand Flat Airstrip:    [26585]

  243:      Judy Tank:    [135990]
            Bryan Canyon Tank:    [63515]
            Whiting Tank:    [27900]
            White Tank:    [7251]

  247:      Mineral Springs:    [120980]
            Toro Tank:    [112003]
            Harris Bear Spring:    [111722]
            Price Smith Tank:    [61554]
            Sertion Three Tank:    [60267]
            Martin Tank:    [46914]
            Tom Tank:    [45852]

  248:      Jacks Creek Campground:    [84938]
            Bear Canyon Corral:    [16319]

  251:      South Cisneros:    [114302]
            Sapo Tank:    [109175]
            Hudson Spring:    [81491]
            Lower Holdup Tank:    [80532]
            Barela Number Two Tank:    [61260]
            Bicara Tank:    [60984]
            Peak Tank:    [27491]
            Wee Tank:    [19136]

  252:      Lower Skates Corral:    [88770]
            Bernalillo Watershed Withdrawal:    [2672]

  253:      Cold Spring Dam:    [83750]
            Long Ridge Electronic Site:    [24660]

  255:      Deer Spring:    [71717]
            Royal Tank:    [66659, 130595]
            S U Tank Number Two:    [18997]

  257:      McCarley:    [51388]

  259:      Engineer Tank:    [134770]
            Llano Tank:    [74516]
            Little Cherry Spring:    [21833]
            Eagle Tank:    [10904]

  260:      APS Camp:    [126327]
            Highline Trail:    [75338]
            Ortiz Well:    [58782]

  261:      Holy Ghost Summer Home Area:    [58369]

  263:      Conjilon Tank:    [108900]
            Railroad Tank:    [72246]
            Lower Aqua Tank:    [56914]
            Lower Plaza Tank Number Two:    [19549]

  264:      Comanche Well:    [72386]

  265:      Eds Place:    [86404]

  267:      Cow Creek Tank:    [64210]

  270:      Denton Canyon:    [3152]

  271:      McKenzie Tank:    [122373]
            Lower Abrevadero Tank:    [121548]
            Number Four Tank:    [116886]
            Earth Tank Number Two:    [116197]
            Cerro Spring:    [101687]
            Daniel Tank:    [65825]
            Bear Tank:    [65286]
            Ruidoso Downs Race Track:    [32879]
            Hidden Spring:    [30836]
            Nancy Tank:    [27215]
            Wild Boy Spring:    [23167]

  272:      Department of Defense Withdrawal:    [4522]

  273:      Questecita Blanca:    [124468]
            Mathews Tank Number Three:    [45264]

  275:      Aspen Spring:    [82691]
            Turkey Spring:    [65149]

  278:      Taos Ranger Station:    [70351]
            Pecos District Ranger Station:    [59567]

  279:      Felipe Tank:    [113113]
            Montosa Tank:    [76324]
            Stover Tank:    [72793]
            Camposanto Jesus Nazareno:    [71188]
            Bear Wallow Trick Tank:    [71040]
            Six Bar Tank:    [14815]
            Escondido Spring:    [6027, 7910, 99571]

  280:      Holy Ghost Campground:    [84807]

  281:      Dry lake:    [49244]

  283:      Aranda Tank:    [101420]
            Gravel Tank:    [88094]
            E Robertson Tank:    [26709]

  287:      Reveg Tank:    [73579]
            TCLP Tank:    [73181]
            Taos:    [69565]
            Saliz Tank:    [15244]

  288:      Cow Creek Campground:    [58090]
            Water Hole Well:    [34043]
            Oak Grove Campground:    [33284]

  289:      West Cow Water Storage:    [42154]

  291:      Cedro Tank:    [124858]
            Gurule Spring:    [104298]
            Broke Off Tank:    [97541]
            Pinabetosa Trick Tank:    [97132]
            Torres Tank:    [72661]
            La Jara Tank:    [45987, 99009]
            Big Tank:    [35267, 38059]

  292:      Puerco Espin Trail:    [75609]
            Ruidoso Municipal Airport:    [32351]
            Palacio Trail:    [4156]

  295:      Chaves Trick Tank:    [129649]
            Indio Spring:    [107773]
            Lower Lopez Tank:    [96455]
            Lost Tank:    [93392]
            Burkes:    [49479]
            Logan Tank:    [30035]

  296:      Fox Corral:    [134405]
            Ojitas Polvadera Trail:    [75470]

  297:      Willie Kelly Place:    [15916]

  299:      Dead Deer Tank:    [129508]
            Old Trick Tank:    [119851]
            Underwood Spring Number Two:    [108464]
            Sambrano Tank:    [64351]
            Section Thirty One Tank:    [62392]

  300:      Grant Corral:    [129251]

  301:      Grass Mountain Summer Home Area:    [84402]
            Pit Tank Number One:    [557]

  303:      Hartig Tank:    [77278]
            Driveway Tank:    [1723, 92712]

  304:      Lower Windmill:    [91634]

  305:      Angle Fire:    [68164]

  306:      Hornbuckle Canyon:    [20232]

  307:      Dugout Spring:    [125398]
            Comanche Tank:    [123482]
            CCC Tank:    [39753]
            Pine Spring:    [21295, 92852]
            Turkey Roost Tank:    [21158]

  309:      Aspen Vista Picnic Area:    [53548]

  311:      Canyon Tank:    [91762]
            Upper Holdup Tank:    [80673]
            Fuente Trick Tank:    [69072]
            Tub Spring:    [15381]

  312:      Table Top:    [21032]
            Camp Campbell:    [8480]

  315:      Clara Peak Trick Tank:    [125916]
            Apache Spring:    [59029]
            Hondo Tank:    [35812]
            Graves Tank:    [34989]
            Corner Tank Number Two:    [28330, 113995]
            Pasture Ridge Tank:    [23029]
            Dripping Spring:    [21422, 24520]
            Ojo Medio:    [1862]

  316:      Bone Ranch:    [131452]
            Aragon Well:    [47462]
            Saliz Corral:    [16711]
            Apache Creek Deaf Ranch:    [9313]

  318:      Trail Canyon:    [43080]

  319:      Show Tank:    [122510]
            Moeller Tank:    [98709]
            Porky Spring:    [70097]
            Hay Gulch Tank:    [62951]
            Carpenter Tank:    [62809]
            Higgins:    [49597]
            Joyce Water Storage Tank:    [24114]

  320:      San Juan Corral:    [84273]

  321:      Cisnero Mine Number One:    [74382]

  323:      Earth Tank:    [130217, 132290]
            Barley Spring:    [126456]
            Norman Spring:    [50861]
            Prather Tank:    [40445]

  325:      Welch Lodge:    [28602]

  327:      Prestridge Spring:    [30971]
            Rito Cola Largo:    [2811]
            Julian Number Two Tank:    [2521]

  329:      Skates Tank Number One:    [87820]

  331:      Cedar Tank:    [62537, 128970]
            Rico Tank:    [61838]
            Quick Tank:    [46663]
            Middle Poe Tank:    [44854]
            Indian Joe Tank:    [1008]

  332:      Borrego Mesa Campground:    [54472]
            West Sawtooth Well:    [1990]

  333:      Borracho Forest Service Facility:    [123753]

  335:      Fork Tank:    [132928]
            Willow Tank:    [128827]
            Wild Horse Tank:    [120269]
            Dunlap Spring:    [99706]
            Triangle Tank:    [68286]
            Two Trough Spring:    [52611]
            Section Eleven Tank:    [39609]
            Chimney Canyon Tank:    [14665]
            Oso Spring:    [282]

  336:      Mesa Vista School:    [120847]

  337:      FAA Electronic Site:    [74799]

  339:      Bean Patch Tank:    [113681]
            Dan Johnson Tank:    [61409]

  341:      Rio Chiquito Picnic Area:    [70218]

  343:      Juan Spring:    [100241]

  344:      Old Helper Ranch:    [39350]

  347:      Beehive Tank:    [128683]
            Barranca Tank:    [122777]
            Elk Spring:    [111587]
            Diego Spring:    [101819]
            Montoya Spring:    [96596]
            Capulin Spring:    [71595]
            Nome Tank:    [37170]
            Brushy Tank:    [14395]
            Pine Tank:    [11443, 29634, 32618, 94913]
            Diablo Spring:    [5615]

  348:      Herring Ranch:    [50108]
            Langford Windmill:    [31233]

  349:      Jaramillo Mesa:    [52211]

  350:      Upper Juniper Reservoir:    [45128]

  351:      Canevas Tank:    [112695]
            Bear Spring:    [99435]
            Big Timber Tank:    [88903]
            Heifer Tank:    [88232]
            Jones Spring:    [78502]
            Darkey Tank:    [55545]
            Kiehnes Canyon Tank:    [17650]
            Spur Tank:    [11980]

  353:      Rim Vista:    [126872]
            Buck Mountain Electronic Site:    [36490]

  355:      Lower Cottonwood Tank:    [27752]
            Twin Tank:    [10098]

  357:      Tres Piedras District Ranger Office:    [60833]
            Sierra Blanca Ski Area:    [36907]

  358:      Biscuit Knob:    [87443]

  359:      Chino Tank:    [105671]
            Big Rosa Tank:    [80814]
            Ranch Supply Spring:    [78214]
            West Poe Tank:    [44718]
            Silva Tank:    [37677]
            Frank Tank:    [25904]
            Sally Tank:    [15515]
            Ultimo Water Storage Tank:    [9684]

  360:      Laguna Larga Camgpround:    [105390]

  362:      San Juan Basin:    [51666]

  363:      Jim Tank:    [135046]
            La Cueva Springs:    [124056]
            Ortega Tank:    [121693]
            Lucero Tank:    [114604]
            Trapt Tank:    [37798]

  364:      Bates Windmill:    [50739]
            Spring Seep:    [28977]

  365:      Tanques Cany:    [3985]

  367:      Mule Canyon Tank:    [66100]
            Blanche Tank:    [33145]

  368:      Sweeten Windmill:    [93123]
            Atalaya Trail:    [3312]

  371:      Pat Tank:    [63660]
            Twin Peaks Tank:    [12532]

  372:      Windy Point Vista Point of Interest:    [36631]
            The Roundup Ground:    [25515]

  374:      Cerrito Pelon:    [73843]
            Acequia Madre Del Norte Del Canon:    [70612]

  375:      Loyd Tank:    [129794]
            Salin Tank:    [114752]
            Tule Tank:    [109450]
            Bear Canyon Spring:    [90173]
            Schoolhouse Tank:    [40023]
            Little Spring:    [10370]
            Big Spring:    [6166]

  379:      Pablo Spring:    [100109]
            Borrego Tank:    [73312]

  381:      Trick Tank Number Five:    [95193]
            Guadalupita:    [69450]

  383:      Soloman Tank:    [73445]

  386:      Lower Juniper Reservoir:    [44992]
            Evergreen Hills Subdivision:    [8755]

  387:      El Rito Trick Tank:    [119010]
            Perry Spring Tank:    [34722]
            Pepper Tank:    [33649]
            Lone Pine Tank:    [10506]

  388:      North Side Pine Corral:    [128278]

  390:      Trick Tank Number Four:    [94776]
            Cecil Smith Water:    [28723]

  391:      Canon Largo Tank:    [112835, 123906]
            Patterson Water Storage Tank:    [42935]

  392:      Hop Canyon Well:    [130348]

  395:      Meadow Tank:    [115626]
            Mister Tank:    [106473]
            South Tank:    [106208]
            Soloman Spring:    [70472]
            Telles Spring:    [43353]
            Grainery Tank:    [21980]
            Moore Tank:    [21563]

  396:      Garcia Canyon Well:    [132556]
            Ghost Ranch:    [127277]
            Drake Ranch:    [76873]

  399:      Pinon Tank:    [118082]
            Bug Tank:    [107643]
            Nester Spring:    [100628]
            West Boundary Tank:    [98126]
            Guid Tank:    [65963]
            Line Water Storage Tank:    [41632]
            Karruth Tank Number Two:    [25094]
            Paddle Cross Tank:    [1440]

  400:      T-Bone Ranch:    [115184]

  401:      Terry Tank Number Three:    [89740]
            Cherry Lake:    [36220]

  403:      Red Bluff Tank:    [33014]
            Little Joe Trick Tank:    [16833]

  404:      Bonito Seep:    [35948]

  405:      Eturriage:    [107395]

  407:      Canada Ojitos:    [102208]
            Sierra Mosca Trick Tank:    [54322]
            Alamo Tank:    [47716, 107131, 110896]

  411:      Pot Tank:    [135311]
            Redtop Spring:    [126737]
            Alamosa Number One Tank:    [118718]
            East Fork Polvadera Creek:    [76145]
            Upper Long Tank:    [28849]
            Cibola National Forest Supervisors Headquarters:    [8314]

  412:      Granans Ranch:    [50236]
            Fairchild Well:    [31366]

  414:      Webster:    [51272]

  415:      Juniper Tank:    [80955]
            BS Tank:    [52341]
            Godley Tank:    [44356]

  416:      North Well:    [41770]

  417:      Erin Lake:    [108037]

  418:      Nerio Canyon:    [95765]

  419:      High Clark Spring:    [133059]
            Hells Hole Tank:    [128546]
            Aquaje Tank:    [113533]
            Weener Spring:    [86138]
            Flechado Trick Tank:    [68552]
            Petaca Tank:    [63376]
            Cave Spring:    [5357]

  420:      Park Well:    [81233]
            Rechuelos Trail:    [75885]
            Clayton Cattle Feeders Incorporated Feed Lot:    [51504]
            Stroope Well:    [49714]
            Agua Piedra Trail:    [48666]

  421:      El Dorado at Santa Fe:    [126048]

  423:      Moeller Spring:    [131995]
            La Jolla Trick Tank:    [124721]
            Oso Tank:    [124203]
            Tower Peak:    [81765]
            Sand Hills:    [4398]
            Red Peak:    [2113]

  424:      Salitral Corral:    [103314]
            Skyline Campground:    [33409]

  427:      Miners Tank:    [130884]
            Mars Tank:    [106076]
            Bonito Spring:    [96866]
            Grayback Tank:    [12963]
            Upper Fourth of July Spring:    [6437]

  429:      Fitzgerald Cienega:    [26329]

  430:      Nave Canyon:    [91100]

  431:      Hell Canyon Tank:    [110633]
            La Cueba Tank:    [104571]
            Sandy Tank:    [76746]
            Munoz Tank:    [37416]

  433:      Underwood Spring Number One:    [108319]

  434:      Dry Canyon:    [98577]

  435:      Snare Mesa Tank:    [135573]
            Campbell Tank:    [118460]
            Aranda Spring:    [101554]
            Divided Tank:    [93512]
            Gurule Tank:    [66520]
            Junior Tank:    [45577]
            Crossing Tank:    [12678]

  436:      Gap Well:    [82574]
            Cooney Gap:    [79991]

  437:      Spring Number One:    [94030]

  439:      No Permit Tank:    [135711]
            Burma Trick Tank:    [121259]
            Grindstone Spring:    [57291]
            Mojino Tank:    [55949]
            Laney Spring:    [31491]
            Upper Devils Tank:    [12384]
            Cedro Trick Tank:    [7645]

  440:      La Jara Wash:    [76988]

  441:      Canjilon District Ranger Office:    [108757]

  443:      Placito Canyon Tank:    [98423]
            Loco Tank:    [89045]
            Mortondad Spring:    [58512]
            Lumber Tank:    [47581]
            Red Tank:    [18076]

  446:      Airplane Canyon:    [19696]

  447:      Prairie Dog Trick Tank:    [119561]
            Number Three Tank:    [115774]
            Salazar Tank:    [102742]
            Welty Tank:    [87310]
            Goldylocks Water Storage Tank:    [42789]
            Black Gap Tank Number Two:    [28039]

  449:      White Place:    [129382]
            Kiowa and Rita Blanca Ranger District Office:    [48928]

  450:      Soldier Canyon:    [43744]

  451:      West Red Tank:    [86268]
            Shoe Tank:    [80255]
            Yellowstone Spring:    [5751]
            Monument Tank:    [1583]

  455:      Balkie Tank:    [19980]

  456:      Whitaker Ranch:    [9447]

  457:      Windy Bridge Recreation Site:    [57687]
            Lower Plaza Tank Number One:    [18203]

  458:      Bryan:    [64893]

  459:      Yolanda Spring:    [131709]
            Chino Peak Tank:    [105013]
            Exter Tank:    [81095]
            John Tank:    [61123]
            Corner Tank:    [55811, 117304, 134281]
            Bailey Tank:    [55683]
            Arroyo Plaza Tank:    [51938]
            Lardhole Tank:    [44589]
            Matney Spring:    [30579]
            Five Springs Tank:    [17799]

  460:      Tajique Campground:    [6724]

  461:      Llanito Frio Recreation Site:    [68814]
            Sierra Blanca Ski Lodge:    [36774]
            Burleson Ridge:    [24801]

  463:      Vito Tank:    [127554]
            Madrid Tank:    [127409]
            Vaquero Tank:    [111172]
            Miller Tank:    [60555]
            Hale Canyon Tank:    [32485]
            Russ Tank:    [13546]
            Ojo Terreo Spring:    [6984]

  466:      Magdalena Ranger Station:    [111453]

  467:      Brown Tank:    [132682]
            Winter Spring:    [125525]
            Manzanares Tank:    [120127]
            Puertecito Tank:    [112140]
            Pedro Spring:    [99839]
            South Trick Tank:    [95340]
            Geronimo:    [84549]
            Cyanide Tank:    [80392]
            Valle De Las Romeros:    [69678]
            Comanche Number One Tank:    [66938]
            East Monticello Spring:    [47320]
            Draw Tank:    [26953]
            Corner Dry Pasture Tank:    [20403]
            Saddle Tank:    [18591, 92448]

  468:      Upper Logunitas Campground:    [56509]
            Brunson Ranch:    [46280]

  470:      Alfred Hale Canyon:    [31893]

  471:      North Cisneros:    [114453]
            Bernardo Tank:    [97690, 99156]
            Rocky Tank:    [73049]
            Deep Tank:    [65013]

  475:      Vacus Spring:    [126193]
            Gibbons Lake Tank:    [108609]
            Monica Spring:    [79152]
            Valdez Tank:    [62115, 109312]
            Storm Tank:    [36351]
            Lucas Tank:    [34325]
            B-Storage Tank:    [23445]

  476:      New Well:    [78631, 125796]

  477:      Tilted Rock Picnic Area:    [73976]
            Comales Sheep Driveway:    [68035]

  478:      Long Canyon:    [3695]

  479:      Board Tank:    [30312]
            Gap Tank:    [28467, 77803]
            Spring Tank:    [18470, 63093]

  481:      Pendaries Village:    [86001]
            Oak Flat District Ranger Office:    [47042]
            Pasture Ridge:    [22900]

  483:      McDonald Spring:    [130470]
            Kenyon Tank:    [123619]
            Middle Tank:    [106607]
            Sandy Deer Spring:    [104867]
            Olguin Mesa Tank:    [94180]
            Wayne Tank:    [89603]
            Kellog Tank:    [82299]
            Cement Tank:    [79429]
            Pueblo Park Spring:    [16447]
            Kelly Brushy Tank:    [13970]

  487:      Brand Tree Canyon Tank:    [128133]
            Servilleta Tank:    [56087]
            Hick Water Storage Tank:    [12115]

  489:      Earth Tank Number One:    [117032]

  491:      Pinta Tank:    [124996]
            Rim Tank:    [112283, 127991]
            TIp Top Spring:    [110509]
            Lino Spring:    [103027]
            Blas Tank:    [96732]
            Monica Tank:    [78753]
            Fifteen Tank:    [27352, 109038]
            Snow Tank:    [12244]

  494:      Shipman Cabin:    [90977]

  495:      Gato Tank:    [127134]
            Pinon Number One Tank:    [119707]
            Sawmill Tank:    [105807]
            Jarosito Trick Tnak:    [101163]
            East Trick Tank:    [95625]
            Turkey Cienega Tank:    [89890]
            Fence Tank:    [38317]
            Wood Tank:    [29898]
            Javeling Tank:    [16585]
            Apache Peak Pit Tank:    [13107]

  496:      Willow Creek Campground:    [84675]
            Sweethart Windmill:    [37041]

  499:      Monticello Number Two Tank:    [47984]

