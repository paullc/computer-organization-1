Executing gradeC08a.sh...

Creating build subdirectory

Copying student source file to the build directory:
cp StringHashTable.c ./build/StringHashTable.c

Unpacking test code into build directory:
c08adriver.c
NM_1000.txt
StringHashTable.h
testHashTable.h
testHashTable.o
Compiling test code and submission
gcc -o c08a_StringHashTable -std=c11 -W -Wall -ggdb3 c08adriver.c StringHashTable.c testHashTable.o

======================================================================
