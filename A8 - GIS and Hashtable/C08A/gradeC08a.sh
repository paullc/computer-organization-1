#! /bin/bash
#
#  Last modified:  April 14, 2020
#
#  Invocation:  ./gradeC08a.sh <name of your .c file>
#               e.g., ./gradeC08a.sh wmcquain.c
#
#  Alternate invocation:  ./gradeC08a.sh -clean
#
#     This will remove the files created by an earlier run of the grading
#     script; it's useful if you just want to start with a pristine
#     environment.
#
#  Using the script:
#    - copy the GIS data file you want to use into the grading directory
#    - if needed, edit the initialization of gisFile below
#    - copy your version of StringHashTable.c into the grading directory;
#      name it PID.c, where PID is your VT email PID
#    - run: ./gradeC08a.sh PID.c
#    - examine the file PID.txt for scoring details;
#      if that file does not exist, examine buildLog.txt
#
#  Name of grading package file
gradingTar="c08aGrader.tar"

#  Rename for student file
renameC="StringHashTable.c"

#  Name of GIS data file used for input
gisFile="NM_1000.txt"

#  Names for generated files
resultsFile="scores.txt"
testOut1="buildTest.txt"
testOut2="searchTest.txt"
testOut3="table.txt.txt"
testOut4="scores.txt"

#  Names for directories
buildDir="build"
valgrindDir="valgrindTesting"

#   Names for log files created by this script:
buildLog="buildLog.txt"
testLog="details.txt"

#   Name for the executable
exeName="c08a"
seedFile="seed.txt"

#   Delimiter to separate sections of report file:
Separator="======================================================================"

############################################# fn to check for tar file
#                 param1:  name of file to be checked
isTar() {

   mimeType=`file -b --mime-type $1`
   if [[ $mimeType == "application/x-tar" ]]; then
      return 0;
   fi
   if [[ $mimeType == "application/x-gzip" ]]; then
      return 0;
   fi
   return 1;
}

##################################### fn to extract token from file name
#                 param1: (possibly fully-qualified) name of file
#  Note:  in production use, the first part of the file name will be the
#         student's PID
#
getPID() { 

   fname=$1
   # strip off any leading path info
   fname=${fname##*/}
   # extract first token of file name
   sPID=${fname%%.*}
}
   
############################################################ Validate command line

# Verify number of parameters
   if [[ $# -ne 1 ]] && [[ $# -ne 2 ]]; then
      echo "You must specify the name of your C file (or -clean)."
      exit 1
   fi
   
# See if user selected the cleaning option
  if [[ $1 == "-clean" ]]; then
     echo "Removing earlier test files..."
     rm -Rf *.txt $buildDir driver $valgrindDir
     exit 0
  fi

# Verify presence of named file
   sourceFile="$1"
   sourceFile=${sourceFile##*/}
   if [ ! -e $sourceFile ]; then
      echo "The file $sourceFile does not exist."
      exit 2
   fi
    
   appendSwitch="no"
   if [[ $# -eq 2 ]] && [[ $2 == "-append" ]]; then
      appendSwitch="yes"
   fi
   
############################################################ Check for grading package

   if [[ ! -e $gradingTar ]]; then
      echo "$gradingTar is not present"
      exit 3
   fi
   
   isTar $gradingTar
   if [[ $? -ne 0 ]]; then
      echo "$gradingTar does not appear to be a tar file"
      exit 4
   fi

############################################################ Get student's PID
   
   # Extract first token of C file name (student PID when we run this)
   getPID $sourceFile
   
   # Initiate header for grading log
   headerLog="header.txt"
   echo "Grading:  $1" > $headerLog
   echo -n "Time:     " >> $headerLog
   echo `date` >> $headerLog
   echo >> $headerLog
   
############################################################ Prepare for build
  
   # Create log file:
   echo "Executing gradeC08a.sh..." > $buildLog
   echo >> $buildLog
   
   # Create build directory:
   echo "Creating build subdirectory" >> $buildLog
   echo >> $buildLog
   # Create build directory if needed; empty it if it already exists
   if [[ -d $buildDir ]]; then
      rm -Rf ./$buildDir/*
   else
      mkdir $buildDir
   fi
   
   # Copy student's C file to the build directory
   echo "Copying student source file to the build directory:" >> $buildLog
   echo "cp $sourceFile ./$buildDir/$renameC" >> $buildLog
   cp $sourceFile ./$buildDir/$renameC >> $buildLog 2>&1
   echo >> $buildLog
   
   # Unpack the test source files into the build directory
   echo "Unpacking test code into build directory:" >> $buildLog
   tar xvf $gradingTar -C ./$buildDir >> $buildLog 2>&1
   
   # Move the GIS record file into the parent directory
   mv ./$buildDir/$gisFile .

   # Move to build directory
   cd ./$buildDir
   
############################################################ Build the test driver

   # build the driver; save output in build log
   echo "Compiling test code and submission" >> ../$buildLog
   exeName=$exeName"_"$sPID
   buildCMD="gcc -o $exeName -std=c11 -W -Wall -ggdb3 c08adriver.c $renameC testHashTable.o"
   echo $buildCMD >> ../$buildLog
   
   $buildCMD >> ../$buildLog 2>&1
   echo >> ../$buildLog

   # Verify existence of executable
   if [[ ! -e $exeName ]]; then
      echo "Build failed; the file $exeName does not exist" >> ../$buildLog
      echo $Separator >> ../$buildLog
      mv ../$buildLog ../$sPID.txt
      exit 7
   fi
   if [[ ! -x $exeName ]]; then
      echo "Permissions error; the file $exeName is not executable" >> ../$buildLog
      echo $Separator >> ../$buildLog
      mv ../$buildLog ../$sPID.txt
      exit 8
   fi

   echo "Build succeeded..." >> $buildLog
   
   # Move executable up to test directory and return there
   echo "Moving the executable $exeName to the test directory." >> $buildLog
   mv ./$exeName ..
   # Move seed file to test directory
   if [[ ! -e $seedFile ]]; then
      echo 98090934 > $seedFile
   fi
   mv $seedFile ..
   
   # Return to test directory
   cd .. >> $buildLog
   
   # Delimit this section of the report file:
   echo $Separator >> $buildLog

############################################################ Perform testing
   
   # Execute the test code
   killed="no"
   timeout -s SIGKILL 30 ./$exeName $gisFile -repeat > $testLog 2>&1
   
   if [[ $timeoutStatus -eq 124 || $timeoutStatus -eq 137 ]]; then
      killed="yes"
      echo "The test of your solution timed out after 30 seconds." >> $testLog
   elif [[ $timeoutStatus -eq 134 ]]; then
      killed="yes"
      echo "The test of your solution was killed by a SIGABRT signal." >> $testLog
      echo "Possible reasons include:" >> $testLog
      echo "    - a segfault error" >> $testLog
      echo "    - a serious memory access error" >> $testLog
   fi
   
   # Check for existence of student output file
   if [[ ! -e $resultsFile ]]; then
      echo "Score results file was not found!" > $resultsFile
      echo ">>Scores from testing<<" >> $testLog
      echo "1 >> Score for building:     0.00" >> $resultsFile
      echo "2 >> Score for searching:    0.00" >> $resultsFile
      echo "3 >> Weighted score:         0.00" >> $resultsFile
   fi

############################################################ Run valgrind check
   # run valgrind check iff killed == "no" after grading run above
   if [[ $killed = "no" ]]; then
      # create subdirectory for valgrind testing (avoid overwriting test output)
      mkdir ./$valgrindDir
      cp $exeName ./$valgrindDir
      cp $gisFile ./$valgrindDir
      cp $seedFile ./$valgrindDir
      cd ./$valgrindDir
      
	   #   full valgrind output is in $vgrindLog
	   #   extracted counts are in $vgrindIssues
	   vgrindLog="vgrindLog.txt"
	   echo "Running valgrind test..." >> $vgrindLog
	   vgrindSwitches=" --leak-check=full --show-leak-kinds=all --log-file=$vgrindLog --track-origins=yes -v"
	   scoreResultsLog2="ScoresValgrind.txt"
	   tmpVgrind="tmpVgrind.txt"
	   timeout -s SIGKILL 30 valgrind $vgrindSwitches ./$exeName $gisFile -repeat >> $tmpVgrind 2>&1
		timeoutStatus="$?"
		# echo "timeout said: $timeoutStatus"
		if [[ $timeoutStatus -eq 124 || $timeoutStatus -eq 137 ]]; then
			echo "The test of your solution timed out after 60 seconds." >> $testLog
			echo "Valgrind testing will NOT be completed." >> $testLog
			killed="yes"
		elif [[ $timeoutStatus -eq 134 ]]; then
			echo "The test of your solution was killed by a SIGABRT signal." >> $testLog
			echo "Possible reasons include:" >> $testLog
			echo "    - a segfault error" >> $testLog
			echo "    - a serious memory access error" >> $testLog
			echo "Valgrind testing will NOT be completed." >> $testLog
			killed="yes"
		fi
	   if [[ -s $tmpVgrind ]]; then
	      cat $tmpVgrind >> $vgrindLog
	   fi
	   cp $vgrindLog ..
	   
	   # accumulate valgrind error counts
	   if [[ -e $vgrindLog ]]; then
	      vgrindIssuesLog="vgrind_issues.txt"
	      vgrindIssues=$vgrindIssuesLog
	      echo "Valgrind issues:" > $vgrindIssues
	      grep "in use at exit" $vgrindLog >> $vgrindIssues
	      grep "total heap usage" $vgrindLog >> $vgrindIssues
	      grep "definitely lost" $vgrindLog >> $vgrindIssues
	      echo "Invalid reads: `grep -c "Invalid read" $vgrindLog`" >> $vgrindIssues
	      echo "Invalid writes: `grep -c "Invalid write" $vgrindLog`" >> $vgrindIssues
	      echo "Uses of uninitialized values: `grep -c "uninitialised" $vgrindLog`" >> $vgrindIssues
	      cp $vgrindIssuesLog ..
	   else
	      echo "Error running Valgrind test." >> $testLog
	   fi
	   
	   cd ..
   fi

############################################################ File report
# complete summary file

   summaryLog="$sPID.txt"
   
   # write header to summary log
   cat "$headerLog" > $summaryLog
   echo $Separator >> $summaryLog
   
   # log score data in format for harvesting
   cat $resultsFile >> $summaryLog
   echo $Separator >> $summaryLog

   # write valgrind summary into summary log   
   if [[ $killed = "no" ]]; then
	   # write Valgrind summary into log
	   echo "Summary of valgrind results:" >> $summaryLog
	   echo >> $summaryLog
	   cat $vgrindIssuesLog >> $summaryLog
	   echo $Separator >> $summaryLog
	else
	   echo "No valgrind test results are available" >> $summaryLog
	   echo $Separator >> $summaryLog
   fi
   
   # write test logs to summary log
   if [[ -e $testOut1 ]]; then
      echo "Test results for building the hash table:" >> $summaryLog
      echo >> $summaryLog
      cat $testOut1 >> $summaryLog
   else
      echo "No test results were found for building the hash table!" >> $summaryLog
   fi
   echo $Separator >> $summaryLog

   if [[ -e $testOut2 ]]; then
      echo "Test results for searching the hash table:" >> $summaryLog
      echo >> $summaryLog
      cat $testOut2 >> $summaryLog
   else
      echo "No test results were found for searching the hash table!" >> $summaryLog
   fi
   echo $Separator >> $summaryLog

   if [[ -e $testOut3 ]]; then
      echo "Here's a display of the contents of your hash table:" >> $summaryLog
      echo >> $summaryLog
      cat $testOut3 >> $summaryLog
   else
      echo "No display was generated for your hash table!" >> $summaryLog
   fi
   echo $Separator >> $summaryLog

   # write full valgrind log into summary   
   if [[ $killed = "no" ]]; then
	   # write Valgrind details into log
	   echo "Details from valgrind check:" >> $summaryLog
	   echo >> $summaryLog
	   cat $vgrindLog >> $summaryLog
	   echo $Separator >> $summaryLog
	else
	   echo "No valgrind details are available" >> $summaryLog
	   echo $Separator >> $summaryLog
   fi

   # write build log into summary
   echo "Results from $buildLog" >> $summaryLog
   cat $buildLog >> $summaryLog
   echo >> $summaryLog
   
   # if requested, append student's code to report file
   if [[ $appendSwitch == "yes" ]]; then
      echo "Student's submitted code:" >> $summaryLog
      echo >> $summaryLog
      cat $sourceFile >> $summaryLog
      echo $Separator >> $summaryLog
   fi

exit 0
