#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include "StringBundle.h"
#include "StringHashTable.h"

// On my honor:
//
// - I have not discussed the C language code in my program with
// anyone other than my instructor or the teaching assistants
// assigned to this course.
//
// - I have not used C language code obtained from another student,
// the Internet, or any other unauthorized source, either modified
// or unmodified.
//
// - If any C language code or documentation used in my program
// was obtained from an authorized source, such as a text book or
// course notes, that has been clearly noted with a proper citation
// in the comments of my program.
//
// - I have not designed this program in such a way as to defeat or
// interfere with the normal operation of the grading code.
//
// <Pau Lleonart Calvo>
// <paullc>

// Node type for the chained hash table for C-strings.
struct _StringNode
{
	char *key;				  // ptr to proper C-string
	uint32_t *locations;	  // ptr to dynamic array of string locations
	uint32_t numLocations;	  // number of elements currently in locations array
	uint32_t maxLocations;	  // dimension of locations array
	struct _StringNode *next; // ptr to next node object in table slot
};

struct _FidVal
{
	long offset;
	int fid;
};
typedef struct _FidVal FidVal;

uint32_t elfhash(const char *str);

#define PI 3.14159265358979323846
#define RADIUS 6371.0088

// Return the offset of the feature if found or -1 if not found.
int fidCompare(const void *a, const void *b)
{
	FidVal *val_a = (FidVal *)a;
	FidVal *val_b = (FidVal *)b;
	return val_a->fid - val_b->fid;
}

long binSearch(FidVal *array, int numEntries, int featureID)
{
	int low = 0;
	int high = numEntries - 1;

	while (low <= high)
	{
		int mid = (low + high) / 2;

		if (array[mid].fid == featureID)
		{
			return array[mid].offset;
		}

		else if (array[mid].fid < featureID)
		{
			low = mid + 1;
		}
		else
		{
			high = mid - 1;
		}
	}
	return -1;
}

int main(int argc, char **argv)
{

	if(argc != 3) {
		printf("YA MESSED IT UP\n");
		return 1;
	}

	// Opening script and log files
	FILE *script = fopen(argv[1], "r");
	if (script == NULL)
	{
		printf("SCRIPT FAILED TO OPEN");
		return 1;
	}
	FILE *log = fopen(argv[2], "w");
	if (log == NULL)
	{
		printf("LOG FAILED TO OPEN");
		return 1;
	}

	// Getting set to read commands
	char buffer[500];

	fgets(buffer, 500, script);
	while (buffer[0] == ';')
	{
		fgets(buffer, 500, script);
	}

	strtok(buffer, "\t");
	// FREE
	char *fileName = calloc(50, sizeof(char));
	char *f = strtok(NULL, " \n");
	strcpy(fileName, f);

	fgets(buffer, 500, script);
	strtok(buffer, "\t");
	int tableSize = atoi(strtok(NULL, "\n"));

	/**
	 Set up your Hash Table for FNI
	 */

	StringHashTable *FNI = StringHashTable_create(tableSize, *elfhash);

	// Set up FID
	int FIDSize = 256;
	int FIDEntries = 0;
	FidVal *FID = calloc(FIDSize, sizeof(FidVal));

	/**
	 * Set up reading from file for relevant data fields
	 */
	FILE *gisData = fopen(fileName, "r");
	char gisString[500];
	fgets(gisString, 500, gisData);
	long offset = ftell(gisData);
	while (fgets(gisString, 500, gisData) != NULL)
	{

		// Create the bundle of fields
		StringBundle *bundle = createStringBundle(gisString);

		// Add entries to the FID
		if (FIDEntries == FIDSize)
		{
			FIDSize *= 2;
			FID = realloc(FID, FIDSize * sizeof(FidVal));
		}
		FidVal entry;
		entry.fid = atoi(bundle->Tokens[0]);
		entry.offset = offset;
		FID[FIDEntries] = entry;
		FIDEntries++;

		// Get the key for the Hash Table
		char key[500] = {0};
		strncpy(key, bundle->Tokens[1], strlen(bundle->Tokens[1]));
		strncat(key, " ", 1);
		strncat(key, bundle->Tokens[3], strlen(bundle->Tokens[3]));

		// Add entries to the FNI
		StringHashTable_addEntry(FNI, key, offset);
		offset = ftell(gisData);
		clearStringBundle(bundle);
		free(bundle);
	}


	/**
	 * Here we use a quickSort standard function to order the array such that 
	 * we can use a binary search
	 */
	qsort(FID, FIDEntries, sizeof(FidVal), fidCompare);

	int commandNumber = 0;
	while (fgets(buffer, 500, script) != NULL)
	{
		char *tok = strtok(buffer, "\t");
		char *commandName = tok;

		/**
		 Start processing commands!
		 */

		if (strncmp(tok, "exists", strlen("exists")) == 0)
		{
			commandNumber ++;
			/**
			 Handle the "exists" command from the script file
			 */

			// Get the feature and key format
			char *featureName = strtok(NULL, "\t");
			char *state = strtok(NULL, "\n");

			char searchKey[500] = {0};
			strncpy(searchKey, featureName, strlen(featureName));
			strncat(searchKey, " ", 1);
			strncat(searchKey, state, strlen(state));

			// Get locations array
			uint32_t *locations = StringHashTable_getLocationsOf(FNI, searchKey);
			int occurrences = 0;

			while (locations != NULL && *(locations + occurrences) != 0)
			{ //SEGFAULT
				occurrences++;
			}

				// Print out to log
			fprintf(log, "Cmd	%d:	%s	%s	%s\n", commandNumber, commandName, featureName, state);
			fprintf(log, "\n%d occurrences: %s %s\n", occurrences, featureName, state);
			fprintf(log, "----------------------------------------------------------------------\n");

			free(locations);
		}
		else if (strncmp(tok, "details_of", strlen("details_of")) == 0)
		{
			commandNumber++;

			// Get the feature and key format
			char *featureName = strtok(NULL, "\t\n");
			char *state = strtok(NULL, "\n\t"); // CAUSING MISREADS
			char searchKey[500] = {0};
			strncpy(searchKey, featureName, strlen(featureName));
			strncat(searchKey, " ", 1);
			strncat(searchKey, state, strlen(state));

			fprintf(log, "Cmd	%d:	%s	%s	%s\n", commandNumber, commandName, featureName, state);

			// Get locations array
			uint32_t *locations = StringHashTable_getLocationsOf(FNI, searchKey);
			int index = 0;
			while (locations != NULL && *(locations + index) != 0) // size_t n = (&arr)[1] - arr;
			{
				char recordString[500];
				if (fseek(gisData, *(locations + index), SEEK_SET) != 0) {
					printf("FAILED TO SEEK GIS DATA");
				}

				fgets(recordString, 500, gisData);
				StringBundle *recordBundle = createStringBundle(recordString);

				int longDeg, longMin, longSec, latDeg, latMin, latSec;
				sscanf(recordBundle->Tokens[7], "%2d%2d%2d", &latDeg, &latMin, &latSec);
				sscanf(recordBundle->Tokens[8], "%3d%2d%2d", &longDeg, &longMin, &longSec);

				float longCoord, latCoord;
				sscanf(recordBundle->Tokens[9], "%f", &latCoord);
				sscanf(recordBundle->Tokens[10], "%f", &longCoord);

				fprintf(log, "\nFID:	%s\nName:	%s\nType:	%s\nState:	%s\nCounty:	%s\n",
						recordBundle->Tokens[0], recordBundle->Tokens[1], recordBundle->Tokens[2],
						recordBundle->Tokens[3], recordBundle->Tokens[5]);
				fprintf(log, "Longitude: %03dd %dm %0ds West (%f) \n", longDeg, longMin, longSec, longCoord);
				fprintf(log, "Latitude: %dd %dm %ds North (%f)\n", latDeg, latMin, latSec, latCoord);
				index++;
				clearStringBundle(recordBundle);
				free(recordBundle);
				
			}
			fprintf(log, "\n----------------------------------------------------------------------\n");
			free(locations);
		}
		else if (strncmp(tok, "distance_between", strlen("distance_between")) == 0)
		{
			commandNumber++;

			// Get the feature and key format
			int featureID1 = atoi(strtok(NULL, "\t"));
			int featureID2 = atoi(strtok(NULL, "\t"));

			fprintf(log, "Cmd\t%d:\t%s	%d	%d\n\n", commandNumber, commandName, featureID1, featureID2);

			long offset1 = binSearch(FID, FIDEntries, featureID1);
			long offset2 = binSearch(FID, FIDEntries, featureID2);

			float latDec1, longDec1, latDec2, longDec2 = 0;

			char recordString1[500];
			fseek(gisData, offset1, SEEK_SET);
			fgets(recordString1, 500, gisData); //offset 1
			StringBundle *recordBundle1 = createStringBundle(recordString1);
			sscanf(recordBundle1->Tokens[9], "%f", &latDec1);
			sscanf(recordBundle1->Tokens[10], "%f", &longDec1);

			char recordString2[500];
			fseek(gisData, offset2, SEEK_SET);
			fgets(recordString2, 500, gisData); //offset 2
			StringBundle *recordBundle2 = createStringBundle(recordString2);
			sscanf(recordBundle2->Tokens[9], "%f", &latDec2);
			sscanf(recordBundle2->Tokens[10], "%f", &longDec2);

			float latRad1 = latDec1 * (PI / 180);
			float longRad1 = longDec1 * (PI / 180);
			float latRad2 = latDec2 * (PI / 180);
			float longRad2 = longDec2 * (PI / 180);

			int longDeg1, longMin1, longSec1, latDeg1, latMin1, latSec1;
			sscanf(recordBundle1->Tokens[7], "%2d%2d%2d", &latDeg1, &latMin1, &latSec1);
			sscanf(recordBundle1->Tokens[8], "%3d%2d%2d", &longDeg1, &longMin1, &longSec1);

			int longDeg2, longMin2, longSec2, latDeg2, latMin2, latSec2;
			sscanf(recordBundle2->Tokens[7], "%2d%2d%2d", &latDeg2, &latMin2, &latSec2);
			sscanf(recordBundle2->Tokens[8], "%3d%2d%2d", &longDeg2, &longMin2, &longSec2);

			// CALCULATE DISTANCE
			float distance = acos((sin(latRad1) * sin(latRad2)) + (cos(latRad1) * cos(latRad2) * cos(fabs(longRad1 - longRad2)))) * RADIUS;


			fprintf(log, "First:\t( %03dd %dm %ds West, %dd %dm %ds North ) %s, %s\n",
					longDeg1, longMin1, longSec1, latDeg1, latMin1, latSec1,
					recordBundle1->Tokens[1], recordBundle1->Tokens[3]);

			fprintf(log, "Second:\t( %03dd %dm %ds West, %dd %dm %ds North ) %s, %s\n",
					longDeg2, longMin2, longSec2, latDeg2, latMin2, latSec2,
					recordBundle2->Tokens[1], recordBundle2->Tokens[3]);

			fprintf(log, "Distance: %.1fkm\n", distance);
			fprintf(log, "----------------------------------------------------------------------\n");
			clearStringBundle(recordBundle1);
			clearStringBundle(recordBundle2);
			free(recordBundle1);
			free(recordBundle2);
		}
		else
		{ // If this statement executes, then the line was a comment or it was empty

			/**
			 Other conditions, probably some additional else ifs
			 */

			while (tok != NULL)
			{
				fprintf(log, "%s", tok);
				tok = strtok(NULL, "\t");
			}
		}
	}

	/**
	 Clear and free your hash table and FID index
	 */
	StringHashTable_clear(FNI);
	free(FNI);
	free(FID);
	free(fileName);

	fclose(script);
	fclose(log);
	fclose(gisData);

	return 0;
}

/**
 * Hash function
 * @param str string to hash
 * @return hash value
 */
uint32_t elfhash(const char *str)
{
	assert(str != NULL);	//self-destuct if called with NULL
	uint32_t hashvalue = 0, // value to be returned
		high;				// high nybble of current hashvalue
	while (*str)
	{										   // continue until *str is '\0'
		hashvalue = (hashvalue << 4) + *str++; // shift high nybble out,
											   //   add in next char,
											   //   skip to the next char
		if ((high = (hashvalue & 0xF0000000)))
		{										  // if high nybble != 0000
			hashvalue = hashvalue ^ (high >> 24); // fold it back in
		}
		hashvalue = hashvalue & 0x7FFFFFFF; // zero high nybble
	}
	return hashvalue;
}
