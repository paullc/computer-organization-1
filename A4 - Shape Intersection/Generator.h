#ifndef GENERATOR_H_
#define GENERATOR_H_
#include <stdio.h>
#include <stdint.h>

// DO NOT MODIFY THIS FILE IN ANY WAY!! //

/**  Function to create a file of test cases for the rectangle overlap
 *   assignment.
 * 
 *   Pre:     fName stores the name to be used for the test case file
 *            Seed stores the value used to initialize the random
 *              number generator.
 *   Post:    A file containing test cases will have been created.
 *   Returns: The number of test cases generated.  The number of test
 *              cases is hardwired in Generator.c.
 * 
 *   Called by:  main() in driver.c
 *   Calls:      only private helper functions and Std Library functions
 */
uint32_t generateInput(const char* const fName, uint32_t Seed);

/**  Utility function used to write point values for test cases into
 *   the results file. 
 * 
 *   Pre:     Out is open on an output file
 *            Pts specifies the point value to be written
 *   Post:    A string has been written to the output file in the
 *              following format:  "[dd]  ", where dd is the given
 *              point value (truncated to two digits if Pts > 99).
 * 
 *   Called by:  main() in driver.c
 *   Calls:      only Std Library functions
 */
void     writePoints(FILE* Out, uint32_t Pts);

#endif /* GENERATOR_H_ */

