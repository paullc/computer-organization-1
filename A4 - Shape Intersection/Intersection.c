/** CS 2505 Summer 2019:  Intersection.c
 *
 *  Supplied framework for Intersecting Rectangles project.  Your task is to
 *  complete the supplied code to satisfy the posted specification for this
 *  assignment.
 *
 *  Student:   Pau Lleonart Calvo
 *  PID:       paullc
 */
#include "Intersection.h"

// Add any include directives for the Standard Library that your solution needs:

// Declarations for helper functions should go here:

/**  Determines whether two rectangles, A and B, intersect, computes the attributes
 *   of the intersection (if any), and returns true or false accordingly.
 *
 *   Pre:
 *         aSWx and aSWy specify the SW (lower, left) corner of A
 *         aHeight specifies the vertical dimension of A
 *         aWidth specifies the horizontal dimension of A
 *         bSWx and bSWy specify the SW (lower, left) corner of B
 *         bHeight specifies the vertical dimension of B
 *         bWidth specifies the horizontal dimension of B
 *         iSWx and iSWy point to variables the client will use to store the
 *              SW corner of the intersection, if it exists
 *         iHeight and iWidth point to variables the client will use to store
 *              the height and width of the intersection, if it exists
 *
 *   Returns:
 *         true if A and B share at least one point; false otherwise
 */
bool Intersection(int32_t aSWx, int32_t aSWy, uint32_t aHeight, uint32_t aWidth,
                  int32_t bSWx, int32_t bSWy, uint32_t bHeight, uint32_t bWidth,
                  int32_t* const iSWx, int32_t* const iSWy, uint32_t* const iHeight, uint32_t* const iWidth) {

   // Implement the body of this function!

   // If two rectangles do NOT intersect, they will lie within one of these
   // cases.
   if (((aSWx + aWidth < bSWx) || (aSWy + aHeight < bSWy)) ||
      ((bSWx + bWidth < aSWx) || (bSWy + bHeight < aSWy))) {
      
      return false;
   }

   // Find the properties of the intersection since it exists. 
   // The properties of the intersection alawys revolve around the corners of the rectangles.
   // If we evaluate where the rectangles are relative to eachother, then the shared intersection
   // rectangle can be found by finding the max values of the SW corner measurements
   // of each rectangle. This works for both negative and positive numbers since the graph
   //  grows larger in the same directions in both positive and negative realms. 

   // Using ternaries is a simple way to implement maximizing or minimizing functionality. 
   int32_t cSWx = (aSWx > bSWx) ? aSWx : bSWx;
   int32_t cSWy = (aSWy > bSWy) ? aSWy : bSWy;
   int32_t cWidth = (aSWx + aWidth < bSWx + bWidth) ? aSWx + aWidth - cSWx : bSWx + bWidth - cSWx;
   int32_t cHeight= (aSWy + aHeight < bSWy + bHeight) ? aSWy + aHeight - cSWy : bSWy + bHeight - cSWy;

   *iSWx = cSWx;
   *iSWy = cSWy;
   *iWidth = cWidth;
   *iHeight = cHeight;



   return true;

   // On my honor:
   //
   // - I have not discussed the C language code in my program with
   // anyone other than my instructor or the teaching assistants
   // assigned to this course.
   //
   // - I have not used C language code obtained from another student,
   // the Internet, or any other unauthorized source, either modified
   // or unmodified.
   //
   // - If any C language code or documentation used in my program
   // was obtained from an authorized source, such as a text book or
   // course notes, that has been clearly noted with a proper citation
   // in the comments of my program.
   //
   // - I have not designed this program in such a way as to defeat or
   // interfere with the normal operation of the grading code.
   //
   // <Pau Lleonart Calvo>
   // <paullc>
}
