/** CS 2505 Summer 2019:  grading tool for Intersection() function
 *
 *  Execution options:
 *     ./driver <test case file> <results file>
 *     ./driver <test case file> <results file> -repeat
 *
 *  For example:   ./driver tests.txt answers.txt
 *
 *  This will result in the creation of a file named "tests.txt" that
 *  holds specifications for rectanges, and the creation of a file
 *  named "answers.txt" that contains evaluations of the results your
 *  implementation produced on those test cases.
 *
 *  And:   ./driver tests.txt answers.txt -repeat
 *
 *  This will reuse the test data in the file "tests.txt", which was
 *  created by the previous execution of the test driver.  This way,
 *  you can analyze your attempts to fix an error detected when an
 *  earlier set of test cases was used.
 *
 */
#include <stdio.h>         // for I/O functions
#include <stdlib.h>        // generally useful
#include <stdint.h>        // for exact-width integer types
#include <inttypes.h>      // for exact-width integer output
#include <string.h>        // for C-string library
#include <stdbool.h>
#include <time.h>          // for system clock access

#include "Intersection.h"  // for declaration of Intersection()
#include "Generator.h"     // input data generator
#include "checkAnswer.h"   // answer checking code

#define MAXLEN 100         // maximum number of chars in an input line

int main(int argc, char* argv[]) {

   // Make sure we have an acceptable number of command-line arguments, based
   // on the two options given above for running the driver:
	if ( argc != 3 && argc != 4 ) {
      printf("Error:  wrong number of command line parameters.\n");
      printf("Invocation:  %s <test cases file> <results file> [-repeat]\n", argv[0]);
		exit(1);
	}

   // Set the names of the test case and results files:
   char* dataFName = argv[1];        // name of test data file
   char* logFName  = argv[2];        // name of results file

   // Set some variables used in computing your score:
   const uint32_t ptsPerCase = 20;   // number of points per test case
   uint32_t nCases = 0;              // number of test cases used
	uint32_t Score  = 0;              // total score from test cases

   // Access or create test data, according to invocation
   FILE *Data = NULL;
   // If the -repeat switch was given, we are supposed to use an existing
   // file of test data; we'll see if the file exists, and exit with an
   // error message if it doesn't.
   if ( argc == 4 && strcmp(argv[3], "-repeat") == 0 ) {
		Data = fopen(dataFName, "r");
		if ( Data == NULL ) {
			printf("Could not open data file: %s\n", dataFName);
			exit(2);
		}
	}
   // If the user gave a fourth parameter, but it wasn't "-repeat", we'll
   // exit with an error message:
	else if (argc == 4 ) {
		printf("Unrecognized option:  %s\n", argv[3]);
		exit(3);
	}
   // Otherwise, the user did NOT give a fourth parameter, and we will
   // generate new test data:
   else { // argc == 3, so generate random test cases
      uint32_t Seed = (uint32_t) time(NULL);
      generateInput( dataFName, Seed);
		Data = fopen(dataFName, "r");
   }

   // Create file to hold test results
   FILE *Log   = fopen(logFName, "w");

   // Variables used in keeping track if whether the your solution always
   // gives the same answer:
   bool saidTrue  = false;
   bool saidFalse = false;

   // Create a place to store lines read from the test case file...
   char Line[MAXLEN + 1];
   // ... and eat the two header lines (see the specification):
   fgets(Line, MAXLEN + 1, Data);
   fgets(Line, MAXLEN + 1, Data);

   // Variables to store the attributes of the two rectangles we are
   // going to compare:
   int32_t aSWx, aSWy, aHeight, aWidth,   // specification of 1st rectangle
           bSWx, bSWy, bHeight, bWidth;   // specification of 2nd rectangle

   // Write a header for the table of test results we're going to write:
   fprintf(Log, "Rectangle A                         Rectangle B\n");
   fprintf(Log, "SW corner       Height    Width     SW corner       Height    Width\n");
   fprintf(Log, "-------------------------------------------------------------------\n");

   // Read the data for the first pair of rectangles.  The expected formatting is given
   // in the specification.  The return value is the number of values that were actually
   // assigned to our variables.  That had better be 8.
   int numRead = fscanf(Data, "%"PRId32"%"PRId32"%"PRIu32"%"PRIu32"%"PRId32"%"PRId32"%"PRIu32"%"PRIu32,
                              &aSWx, &aSWy, &aHeight, &aWidth, &bSWx, &bSWy, &bHeight, &bWidth);

   // As long as we just obtained the expected number of values, process the test case:
   while ( numRead == 8 ) {

      // Write data for the current rectangles to results file, formatted nicely into
      // a table:
      fprintf(Log, "(%5"PRId32", %5"PRId32")   %5"PRIu32"    %5"PRIu32"     ",
                     aSWx, aSWy, aHeight, aWidth);
      fprintf(Log, "(%5"PRId32", %5"PRId32")   %5"PRIu32"    %5"PRIu32"\n",
                     bSWx, bSWy, bHeight, bWidth);

      // Variables to store attributes of the intersection, if any:
      int32_t solnSWx, solnSWy;
      uint32_t solnHeight, solnWidth;
      bool solnOverlap;

      // Check for intersection:
      if ( (solnOverlap = Intersection(aSWx, aSWy, aHeight, aWidth, bSWx, bSWy, bHeight, bWidth,
                                    &solnSWx, &solnSWy, &solnHeight, &solnWidth)) ) {

         fprintf(Log, "(%5"PRId32", %5"PRId32")   %5"PRIu32"    %5"PRIu32"\n", solnSWx, solnSWy, solnHeight, solnWidth);
         saidTrue = true;
	   }
	   else {
			fprintf(Log, "no intersection\n");
			saidFalse = true;
		}

		// Now, check the answer, and update the score:
		nCases++;
		if ( checkAnswer(aSWx, aSWy, aHeight, aWidth, bSWx, bSWy, bHeight, bWidth,
                       solnOverlap, solnSWx, solnSWy, solnHeight, solnWidth) ) {
         fprintf(Log, "OK, that was correct.\n");
         Score += ptsPerCase;
		}
		else {
         fprintf(Log, "Alas, that was NOT correct.\n");
		}

      // Now, try to read data for the next case; when we get to the end of the test case
      // file, we will read fewer than 8 values here, and that will abort the loop.
      numRead = fscanf(Data, "%"PRId32"%"PRId32"%"PRIu32"%"PRIu32"%"PRId32"%"PRId32"%"PRIu32"%"PRIu32,
                       &aSWx, &aSWy, &aHeight, &aWidth, &bSWx, &bSWy, &bHeight, &bWidth);

      // One point about the logic used in the loop.  By basing the loop exit on whether
      // we have read the expected number of data values for a test case, we guarantee
      // the loop will exit if there's an error in the middle of the test case file.
      //
      // It is also possible to test to see if we've reached the end of the file; you can
      // look up the function feof() if you are curious about that.  I recommend NOT using
      // feof(), because it does not help us if the input file is incorrect in some way,
      // and it may not work in the manner you would expect.
   }

   fprintf(Log, " -------------------------------------------------------------------\n");

   // Check for hardwired response:
   if ( !saidTrue || !saidFalse ) {
		fprintf(Log, "Your implementation always returned the same answer!\n");
		fprintf(Log, "Assigning a score of zero...");
		Score = 0;
	}

   // Report the score:
   fprintf(Log, "\nScore:  %"PRIu32"/%"PRIu32"\n", Score, ptsPerCase*nCases);

   fclose(Data);    // close the input and output files
   fclose(Log);

   // Exit the program:
   return 0;
}
