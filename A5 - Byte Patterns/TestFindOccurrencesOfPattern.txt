Looking for occurrences of the pattern:   57  1D
You said:
   Number of occurrences: 2
   Offsets where found:    19  89
Should be:    Number of occurrences: 2
   Offsets where found:    19  89
That is correct!

Looking for occurrences of the pattern:   87  8F  3C  3A
You said:
   Number of occurrences: 1
   Offsets where found:    1C
Should be:    Number of occurrences: 1
   Offsets where found:    1C
That is correct!

Looking for occurrences of the pattern:   AB  2E  F1  4F
You said:
   Number of occurrences: 2
   Offsets where found:    3D  BC
Should be:    Number of occurrences: 2
   Offsets where found:    3D  BC
That is correct!

Looking for occurrences of the pattern:   B0  E4  EA  F2  54  A5  2B  B0
You said:
   Number of occurrences: 2
   Offsets where found:    90  9B
Should be:    Number of occurrences: 2
   Offsets where found:    90  9B
That is correct!

Looking for occurrences of the pattern:   F3  06  AB  2E  F1  4F  E0  23  D5  C8  70
You said:
   Number of occurrences: 1
   Offsets where found:    3B
Should be:    Number of occurrences: 1
   Offsets where found:    3B
That is correct!

Looking for occurrences of the pattern:   59  D6  E4  AA  80  18  E3  A2  08  B1
You said:
   Number of occurrences: 1
   Offsets where found:    09
Should be:    Number of occurrences: 1
   Offsets where found:    09
That is correct!

Looking for occurrences of the pattern:   E0  23  D5  C8  70  9F  0A  93  4E  F6  36  13  C5  7C  E1  46  C9  26  D1  DE  80  7D  4A  F7
You said:
   Number of occurrences: 0
   Offsets where found:  
Should be:    Number of occurrences: 0
   Offsets where found:  
That is correct!

Looking for occurrences of the pattern:   E2  36  1A  44  59  D6  E4  AA  80  18  E3  A2  08  B2  07
You said:
   Number of occurrences: 0
   Offsets where found:  
Should be:    Number of occurrences: 0
   Offsets where found:  
That is correct!

Looking for occurrences of the pattern:   20  92  C2  F2  70  42  6F  BA  39  5C  F8  38  F3  64  26  DA  B0  39  95
You said:
   Number of occurrences: 0
   Offsets where found:  
Should be:    Number of occurrences: 0
   Offsets where found:  
That is correct!

Looking for occurrences of the pattern:   FE  48  53  3C  A4  1C  4F  FF  FF  FF  FF  FF  FF  FF
You said:
   Number of occurrences: 0
   Offsets where found:  
That is correct!

3 >> Score for findOccurrencesOfPattern:  100 / 100
