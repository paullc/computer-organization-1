Looking for up to 7 occurrences of the byte: 26
        You said:
   Number of occurrences: 3
   Offsets where found:    52  7D  C4
Should be:    Number of occurrences: 3
   Offsets where found:    52  7D  C4
That is correct!

Looking for up to 7 occurrences of the byte: E5
        You said:
   Number of occurrences: 1
   Offsets where found:    D6
Should be:    Number of occurrences: 1
   Offsets where found:    D6
That is correct!

Looking for up to 7 occurrences of the byte: E4
        You said:
   Number of occurrences: 4
   Offsets where found:    0B  91  9C  D3
Should be:    Number of occurrences: 4
   Offsets where found:    0B  91  9C  D3
That is correct!

Looking for up to 7 occurrences of the byte: 59
        You said:
   Number of occurrences: 2
   Offsets where found:    09  AB
Should be:    Number of occurrences: 2
   Offsets where found:    09  AB
That is correct!

Looking for up to 7 occurrences of the byte: 64
        You said:
   Number of occurrences: 2
   Offsets where found:    7C  E4
Should be:    Number of occurrences: 2
   Offsets where found:    7C  E4
That is correct!

Looking for up to 7 occurrences of the byte: BB
        You said:
   Number of occurrences: 1
   Offsets where found:    62
Should be:    Number of occurrences: 1
   Offsets where found:    62
That is correct!

Looking for up to 7 occurrences of the byte: 94
        You said:
   Number of occurrences: 1
   Offsets where found:    BB
Should be:    Number of occurrences: 1
   Offsets where found:    BB
That is correct!

Looking for up to 7 occurrences of the byte: AA
        You said:
   Number of occurrences: 1
   Offsets where found:    0C
Should be:    Number of occurrences: 1
   Offsets where found:    0C
That is correct!

Looking for up to 7 occurrences of the byte: B7
        You said:
   Number of occurrences: 2
   Offsets where found:    F1  F8
Should be:    Number of occurrences: 2
   Offsets where found:    F1  F8
That is correct!

Looking for up to 7 occurrences of the byte: 2B
        You said:
   Number of occurrences: 3
   Offsets where found:    96  A1  E5
Should be:    Number of occurrences: 3
   Offsets where found:    96  A1  E5
That is correct!


1 >> Score for findOccurrencesOfByte:         100 / 100
